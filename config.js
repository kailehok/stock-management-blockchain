var util = require('util');
var path = require('path');
var hfc = require('fabric-client');

var file = 'network-config.yaml';

// indicate to the application where the setup file is located so it able
// to have the hfc load it to initalize the fabric client instance
hfc.setConfigSetting('network-connection-profile-path',path.join(__dirname, 'configuration-profile' ,file));
hfc.setConfigSetting('ad-connection-profile-path',path.join(__dirname, 'configuration-profile', 'ad.yaml'));
hfc.setConfigSetting('od-connection-profile-path',path.join(__dirname, 'configuration-profile', 'od.yaml'));
hfc.setConfigSetting('ep-connection-profile-path',path.join(__dirname, 'configuration-profile', 'ep.yaml'));
// some other settings the application might need to know
hfc.addConfigFile(path.join(__dirname, 'config.json'));
