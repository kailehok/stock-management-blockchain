#########################USAGE#################
#create required fabric containers: 3 orgs (ad and od and ep) with 2 peers each. One SOLO orderer. One ad-cli container. Ccenv (chaincode containers) are dynamically created as chaincodes are instantiated.
#pass "prune" as argument if u want to remove previously created containers, volumes and networks which might interfere with the current network. (eg:previously created channels, instantiated chaincodes,etc)
echo "Ensure you set the IMAGE_TAG environment variable to latest"

CHANNEL_NAME="demo-channel"
COMPOSE_FILE_NAME="compose-files/docker-compose-ad.yaml"
function networkUp() {
    echo "Starting the network and waiting for 10 seconds for network to fully start up"
    docker-compose -f $COMPOSE_FILE_NAME up -d
    sleep 10
}

function createAndJoinChannel() {
    docker exec ad-cli peer channel create -o orderer.demo.com:7050 -c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/channel-artifacts/demo-channel.tx
    #add peer0 from ad org
    docker exec ad-cli peer channel join -b demo-channel.block
    #add peer1 from ad org
    docker exec -e  CORE_PEER_ADDRESS=peer1.ad.demo.com:7051 ad-cli peer channel join -b demo-channel.block
    #add peer from another org
    sleep 5
    docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/crypto/peerOrganizations/od.demo.com/users/Admin@od.demo.com/msp -e CORE_PEER_ADDRESS=peer0.od.demo.com:7051 -e CORE_PEER_LOCALMSPID=odMSP -e CORE_PEER_TLS_ENABLED=false ad-cli peer channel join -b demo-channel.block
    #add peer from another org
    sleep 5
    docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/crypto/peerOrganizations/ep.demo.com/users/Admin@ep.demo.com/msp -e CORE_PEER_ADDRESS=peer0.ep.demo.com:7051 -e CORE_PEER_LOCALMSPID=epMSP -e CORE_PEER_TLS_ENABLED=false ad-cli peer channel join -b demo-channel.block

    #update anchor peers
    sleep 5
    docker exec ad-cli peer channel update -o orderer.demo.com:7050 -c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/channel-artifacts/adMSPanchors.tx
    sleep 5
    docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/crypto/peerOrganizations/od.demo.com/users/Admin@od.demo.com/msp -e CORE_PEER_ADDRESS=peer0.od.demo.com:7051 -e CORE_PEER_LOCALMSPID=odMSP -e CORE_PEER_TLS_ENABLED=false ad-cli peer channel update -o orderer.demo.com:7050 -c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/channel-artifacts/odMSPanchors.tx
    sleep 5
    docker exec -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/crypto/peerOrganizations/ep.demo.com/users/Admin@ep.demo.com/msp -e CORE_PEER_ADDRESS=peer0.ep.demo.com:7051 -e CORE_PEER_LOCALMSPID=epMSP -e CORE_PEER_TLS_ENABLED=false ad-cli peer channel update -o orderer.demo.com:7050 -c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/channel-artifacts/epMSPanchors.tx

}

# OUTPUT=$(docker ps -q)
# if  ! [ -z "$OUTPUT" ]; then
#     echo "Stopping all running containers"
#     docker stop $(docker ps -q)
# fi

sleep 5

if [ "$1" == "prune" ]; then
    echo "Removing all containers, unused volumes and networks"
    docker rm $(docker ps -aq)
    yes | docker volume prune
    # yes | docker network prune
fi
networkUp
createAndJoinChannel
#check if channel is already created. If not create and join peers to it

# OUTPUT=$(docker exec ad-cli peer channel list | grep $CHANNEL_NAME)

# if [ -z "$OUTPUT" ]; then
#     createAndJoinChannel
# fi