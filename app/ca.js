'use strict';
var log4js = require('log4js');
var logger = new log4js.getLogger('Helper');
//logger.setLevel('DEBUG');
logger.level = 'DEBUG';

var path = require('path');
var util = require('util');

var hfc = require('fabric-client');
hfc.setLogger(logger);

var helper = require('./helper.js');

var getRegisteredUsers = async function(userOrg) {
	try {

        var client = await helper.getClientForOrg(userOrg);

		logger.debug('Successfully initialized the credential stores');
			// client can now act as an agent for organization Org1
			// first check to see if the user is already enrolled
		
		
		var admins = hfc.getConfigSetting(userOrg);
		let adminUserObj = await client.setUserContext({username: admins[0].username, password: admins[0].secret});
		let caClient = client.getCertificateAuthority();


		var caIdentity = caClient.newIdentityService()
		var users = await caIdentity.getAll(adminUserObj);

		console.log("\n\n\n\n\n\n\n\n"+JSON.stringify(users)+"\n\n\n\n\n\n\n\n");

		var response;

		if(users.success) {
			response = {
				success: true,
				message: users.result,
				errors: []
			};
		} else {
			response = {
				success: false,
				message: users.messages,
				errors: users.errors
			};
		}

		return response;


	} catch(error) {
		logger.error('Failed to get registered user: %s with error: %s', error.toString());
		return 'failed '+error.toString();
	}

};

getRegisteredUsers('od');