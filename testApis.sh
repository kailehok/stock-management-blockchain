#!/bin/bash
#
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

jq --version > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "Please Install 'jq' https://stedolan.github.io/jq/ to execute this script"
	echo
	exit 1
fi
HOST_NAME="localhost"
starttime=$(date +%s)

# Print the usage message
function printHelp () {
  echo "Usage: "
  echo "  ./testAPIs.sh -l golang|node"
  echo "    -l <language> - chaincode language (defaults to \"golang\")"
}
# Language defaults to "golang"
LANGUAGE="golang"

# Parse commandline args
while getopts "h?l:" opt; do
  case "$opt" in
    h|\?)
      printHelp
      exit 0
    ;;
    l)  LANGUAGE=$OPTARG
    ;;
  esac
done

##set chaincode path
function setChaincodePath(){
	LANGUAGE=`echo "$LANGUAGE" | tr '[:upper:]' '[:lower:]'`
	case "$LANGUAGE" in
		"golang")
		CC_SRC_PATH="stock_management"
		;;
		*) printf "\n ------ Language $LANGUAGE is not supported yet ------\n"$
		exit 1
	esac
}

setChaincodePath

echo "POST request Enroll on AD  ..."
echo
AD_TOKEN=$(curl -s -X POST \
  http://${HOST_NAME}:4000/login \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=mahesh&password=maheshpw&orgName=ad')
echo $AD_TOKEN
AD_TOKEN=$(echo $AD_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo "AD token is $AD_TOKEN"
echo
echo "POST request Enroll on od ..."
echo
OD_TOKEN=$(curl -s -X POST \
  http://${HOST_NAME}:4000/login \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ajaya&password=ajayapw&orgName=od')
echo $OD_TOKEN
OD_TOKEN=$(echo $OD_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo "OD token is $OD_TOKEN"
echo
EP_TOKEN=$(curl -s -X POST \
  http://${HOST_NAME}:4000/login \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=kailash&password=kailashpw&orgName=ep')
echo $EP_TOKEN
EP_TOKEN=$(echo $EP_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo "EP token is $EP_TOKEN"
echo
echo

echo "---------------Registering Users-------------------"
echo
REG_AD=$(curl -s -X POST \
  http://${HOST_NAME}:4000/users \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"username\": \"mahesh1\",
    \"password\": \"mahesh1pw\",
    \"orgName\": \"ad\",
    \"attrs\": [
      {
        \"name\": \"CAN_CREATE_ASSET\",
        \"value\": \"true\",
        \"ecert\": true
      }
    ]
  }"
)
echo $REG_AD
echo


echo "Generate token for an ad user"
AD_TOKEN=$(curl -s -X POST \
  http://${HOST_NAME}:4000/login \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=mahesh1&password=mahesh1pw&orgName=ad')
echo $AD_TOKEN
AD_TOKEN=$(echo $AD_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo "AD token is $AD_TOKEN"
echo
echo



echo "Registering User for od"
echo
REG_OD=$(curl -s -X POST \
  http://${HOST_NAME}:4000/users \
  -H "content-type: application/json" \
  -H "authorization: Bearer $OD_TOKEN" \
  -d "{
    \"username\": \"ajaya1\",
    \"password\": \"ajaya1pw\",
    \"orgName\": \"od\",
    \"attrs\": [
      {
        \"name\": \"CAN_CREATE_ASSET\",
        \"value\": \"true\",
        \"ecert\": true
      }
    ]
  }"
)
echo $REG_OD
echo


echo "Generate token for an od user"
OD_TOKEN=$(curl -s -X POST \
  http://${HOST_NAME}:4000/login \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=ajaya1&password=ajaya1pw&orgName=od')
echo $OD_TOKEN
OD_TOKEN=$(echo $OD_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo "OD token is $OD_TOKEN"
echo
echo



echo "Registering Users for ep"
echo
REG_EP=$(curl -s -X POST \
  http://${HOST_NAME}:4000/users \
  -H "content-type: application/json" \
  -H "authorization: Bearer $EP_TOKEN" \
  -d "{
    \"username\": \"kailash1\",
    \"password\": \"kailash1pw\",
    \"orgName\": \"ep\",
    \"attrs\": [
        {
		\"name\":\"CAN_CREATE_ASSET\",
		\"value\":\"true\",
		\"ecert\" : true
	},
	
	{
		\"name\":\"CAN_CREATE_ASSETTYPE\",
		\"value\":\"true\",
		\"ecert\" : true
	},
	{
		\"name\":\"CAN_UPDATE_ASSET\",
		\"value\":\"true\",
		\"ecert\" : true
	},
	{
		\"name\":\"CAN_CREATE_REQUEST\",
		\"value\":\"true\",
		\"ecert\" : true
	},
	{
		\"name\":\"CAN_CREATE_STATUSTYPE\",
		\"value\":\"true\",
		\"ecert\" : true
	},
	{
		\"name\":\"CAN_UPDATE_REQUEST\",
		\"value\":\"true\",
		\"ecert\" : true
	},
	{
		\"name\":\"CAN_READ_ASSETTYPE\",
		\"value\":\"true\",
		\"ecert\" : true
	},
	{
		\"name\":\"CAN_READ_STATUSTYPE\",
		\"value\":\"true\",
		\"ecert\" : true
	}]
  }"
)
echo $REG_EP
echo


echo "Generate token for an ep user"
EP_TOKEN=$(curl -s -X POST \
  http://${HOST_NAME}:4000/login \
  -H "content-type: application/x-www-form-urlencoded" \
  -d 'username=kailash1&password=kailash1pw&orgName=ep')
echo $EP_TOKEN
EP_TOKEN=$(echo $EP_TOKEN | jq ".token" | sed "s/\"//g")
echo
echo "EP token is $EP_TOKEN"
echo
echo



echo "POST request Create channel  ..."
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/channels \
  -H "authorization: Bearer $AD_TOKEN" \
  -H "content-type: application/json" \
  -d '{
	"channelName":"demo-channel",
	"channelConfigPath":"../channel-artifacts/demo-channel.tx"
}'
echo
echo
sleep 5
echo "POST request Join channel on AD"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/peers \
  -H "authorization: Bearer $AD_TOKEN" \
  -H "content-type: application/json" \
  -d '{
	"peers": ["peer0.ad.demo.com","peer1.ad.demo.com"]
}'
echo
echo

echo "POST request Join channel on OD"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/peers \
  -H "authorization: Bearer $OD_TOKEN" \
  -H "content-type: application/json" \
  -d '{
	"peers": ["peer0.od.demo.com"]
}'
echo
echo

echo "POST request Join channel on EP"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/peers \
  -H "authorization: Bearer $EP_TOKEN" \
  -H "content-type: application/json" \
  -d '{
	"peers": ["peer0.ep.demo.com"]
}'
echo
echo

echo "POST request Update anchor peers on AD"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/anchorpeers \
  -H "authorization: Bearer $AD_TOKEN" \
  -H "content-type: application/json" \
  -d '{
	"configUpdatePath":"../channel-artifacts/adMSPanchors.tx"
}'
echo
echo

echo "POST request Update anchor peers on OD"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/anchorpeers \
  -H "authorization: Bearer $OD_TOKEN" \
  -H "content-type: application/json" \
  -d '{
	"configUpdatePath":"../channel-artifacts/odMSPanchors.tx"
}'
echo
echo

echo "POST request Update anchor peers on EP"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/anchorpeers \
  -H "authorization: Bearer $EP_TOKEN" \
  -H "content-type: application/json" \
  -d '{
	"configUpdatePath":"../channel-artifacts/epMSPanchors.tx"
}'
echo
echo

echo "POST Install chaincode on AD"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/chaincodes \
  -H "authorization: Bearer $AD_TOKEN" \
  -H "content-type: application/json" \
  -d "{
	\"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\"],
	\"chaincodeName\":\"mycc\",
	\"chaincodePath\":\"$CC_SRC_PATH\",
	\"chaincodeType\": \"$LANGUAGE\",
	\"chaincodeVersion\":\"v0\"
}"
echo
echo

echo "POST Install chaincode on OD"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/chaincodes \
  -H "authorization: Bearer $OD_TOKEN" \
  -H "content-type: application/json" \
  -d "{
	\"peers\": [\"peer0.od.demo.com\"],
	\"chaincodeName\":\"mycc\",
	\"chaincodePath\":\"$CC_SRC_PATH\",
	\"chaincodeType\": \"$LANGUAGE\",
	\"chaincodeVersion\":\"v0\"
}"
echo
echo

echo "POST Install chaincode on EP"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/chaincodes \
  -H "authorization: Bearer $EP_TOKEN" \
  -H "content-type: application/json" \
  -d "{
	\"peers\": [\"peer0.ep.demo.com\"],
	\"chaincodeName\":\"mycc\",
	\"chaincodePath\":\"$CC_SRC_PATH\",
	\"chaincodeType\": \"$LANGUAGE\",
	\"chaincodeVersion\":\"v0\"
}"
echo
echo

echo "POST instantiate chaincode on AD"
echo
curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes \
  -H "authorization: Bearer $AD_TOKEN" \
  -H "content-type: application/json" \
  -d "{
  \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\",\"peer0.od.demo.com\",\"peer0.ep.demo.com\"],
	\"chaincodeName\":\"mycc\",
	\"chaincodeVersion\":\"v0\",
	\"chaincodeType\": \"$LANGUAGE\",
	\"args\":[]
}"
echo
echo

# echo "POST invoke chaincode on peers of Org1 and Org2"
# echo
# VALUES=$(curl -s -X POST \
#   http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
#   -H "authorization: Bearer $AD_TOKEN" \
#   -H "content-type: application/json" \
#   -d "{
#   \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\",\"peer0.ep.demo.com\",\"peer0.od.demo.com\"],
#   \"fcn\":\"assettype-create\",
#   \"args\":	{
# 			\"assetType\":\"PERISHABLE\",
	
# }}")
# echo $VALUES

# echo "POST invoke chaincode on peers of Org1 and Org2"
# echo
# VALUES=$(curl -s -X POST \
#   http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
#   -H "authorization: Bearer $AD_TOKEN" \
#   -H "content-type: application/json" \
#   -d "{
#   \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\",\"peer0.ep.demo.com\",\"peer0.od.demo.com\"],
#   \"fcn\":\"asset-create\",
#   \"args\":	{
# 			\"name\":\"Laptop\",
# 			\"description\":\"Lenovo g50\",
# 			\"assetType\":\"PERISHABLE\",
# 			\"qty\":5,
# 			\"price\":50000,
# 			\"expiryDate\":\"2026-01-02T15:04:05+05:45\",
#       \"docType\":\"asset\"
# 		}
# }")
# echo $VALUES
# Assign previous invoke transaction id  to TRX_ID
# MESSAGE=$(echo $VALUES | jq -r ".message")
# TRX_ID=${MESSAGE#*ID:}
# echo



# echo "POST invoke chaincode on peers of Org1 and Org2"
# echo
# VALUES=$(curl -s -X POST \
#   http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
#   -H "authorization: Bearer $AD_TOKEN" \
#   -H "content-type: application/json" \
#   -d "{
#   \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\",\"peer0.ep.demo.com\",\"peer0.od.demo.com\"],
#   \"fcn\":\"asset-create\",
#   \"args\":	{
# 			\"name\":\"Laptop\",
# 			\"description\":\"Lenovo g50\",
# 			\"assetType\":\"PERISHABLE\",
# 			\"qty\":5,
# 			\"price\":50000,
# 			\"expiryDate\":\"2026-01-02T15:04:05+05:45\",
#       \"docType\":\"asset\"
# 		}
# }")
# echo $VALUES

# echo "GET query chaincode on peer1 of AD"
# echo
# OUT=$(curl -s -X GET \
#   "http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc" \
#   -H "authorization: Bearer $AD_TOKEN" \
#   -H "content-type: application/json"
#   -d "{
#   \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\",\"peer0.ep.demo.com\",\"peer0.od.demo.com\"],
#   \"fcn\":\"query\",
#   \"args\":	{
# 			\"selector\": {
# 			\"name\": \"Laptop\"
# 		  }
#   }
# 	}")
# echo $OUT
# echo
# echo

# echo "GET query Block by blockNumber"
# echo
# BLOCK_INFO=$(curl -s -X GET \
#   "http://${HOST_NAME}:4000/channels/demo-channel/blocks/1?peer=peer0.ad.demo.com" \
#   -H "authorization: Bearer $AD_TOKEN" \
#   -H "content-type: application/json")
# echo $BLOCK_INFO
# # Assign previous block hash to HASH
# HASH=$(echo $BLOCK_INFO | jq -r ".header.previous_hash")
# echo

# echo "GET query Transaction by TransactionID"
# echo
# curl -s -X GET http://${HOST_NAME}:4000/channels/demo-channel/transactions/$TRX_ID?peer=peer0.ad.demo.com \
#   -H "authorization: Bearer $AD_TOKEN" \
#   -H "content-type: application/json"
# echo
# echo


# echo "GET query Block by Hash - Hash is $HASH"
# echo
# curl -s -X GET \
#   "http://${HOST_NAME}:4000/channels/demo-channel/blocks?hash=$HASH&peer=peer0.ad.demo.com" \
#   -H "authorization: Bearer $AD_TOKEN" \
#   -H "cache-control: no-cache" \
#   -H "content-type: application/json" \
#   -H "x-access-token: $AD_TOKEN"
# echo
# echo

# echo "GET query ChainInfo"
# echo
# curl -s -X GET \
#   "http://${HOST_NAME}:4000/channels/demo-channel?peer=peer0.ad.demo.com" \
#   -H "authorization: Bearer $AD_TOKEN" \
#   -H "content-type: application/json"
# echo
# echo

echo "GET query Installed chaincodes"
echo
curl -s -X GET \
  "http://${HOST_NAME}:4000/chaincodes?peer=peer0.ad.demo.com" \
  -H "authorization: Bearer $AD_TOKEN" \
  -H "content-type: application/json"
echo
echo

echo "GET query Instantiated chaincodes"
echo
curl -s -X GET \
  "http://${HOST_NAME}:4000/channels/demo-channel/chaincodes?peer=peer0.ad.demo.com" \
  -H "authorization: Bearer $AD_TOKEN" \
  -H "content-type: application/json"
echo
echo

echo "GET query Channels"
echo
curl -s -X GET \
  "http://${HOST_NAME}:4000/channels?peer=peer0.ad.demo.com" \
  -H "authorization: Bearer $AD_TOKEN" \
  -H "content-type: application/json"
echo
echo


echo "Total execution time : $(($(date +%s)-starttime)) secs ..."

sleep 10

CREATE_TYPES=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"type-create\",
    \"args\": {
      \"name\": \"float\"
    }
  }"
)

echo $CREATE_TYPES


CREATE_TYPES=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"type-create\",
    \"args\": {
      \"name\": \"string\"
    }
  }"
)

echo $CREATE_TYPES


CREATE_TYPES=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"type-create\",
    \"args\": {
      \"name\": \"int\"
    }
  }"
)

echo $CREATE_TYPES

CREATE_UNITS=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"unit-create\",
    \"args\": {
      \"name\": \"kg\",
      \"type\": \"float\"
    }
  }"
)

echo $CREATE_UNITS

CREATE_UNITS=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"unit-create\",
    \"args\": {
      \"name\": \"piece\",
      \"type\": \"int\"
    }
  }"
)

echo $CREATE_UNITS

CREATE_UNITS=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"unit-create\",
    \"args\": {
      \"name\": \"liter\",
      \"type\": \"float\"
    }
  }"
)

echo $CREATE_UNITS

CREATE_STATUSTYPE=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"statustype-create\",
    \"args\": {
      \"statusType\": \"WAITING\"
    }
  }"
)

echo $CREATE_STATUSTYPE

CREATE_STATUSTYPE=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"statustype-create\",
    \"args\": {
      \"statusType\": \"APPROVED\"
    }
  }"
)

echo $CREATE_STATUSTYPE

CREATE_STATUSTYPE=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"statustype-create\",
    \"args\": {
      \"statusType\": \"DECLINED\"
    }
  }"
)

echo $CREATE_STATUSTYPE

CREATE_STATUSTYPE=$(curl -s -X POST \
  http://${HOST_NAME}:4000/channels/demo-channel/chaincodes/mycc \
  -H "content-type: application/json" \
  -H "authorization: Bearer $AD_TOKEN" \
  -d "{
    \"peers\": [\"peer0.ad.demo.com\",\"peer1.ad.demo.com\", \"peer0.od.demo.com\", \"peer0.ep.demo.com\"],
    \"fcn\": \"statustype-create\",
    \"args\": {
      \"statusType\": \"DISPATCHED\"
    }
  }"
)

echo $CREATE_STATUSTYPE