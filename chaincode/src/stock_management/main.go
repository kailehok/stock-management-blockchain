/*
 * Copyright 2018 IBM All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"context"
	"fmt"
	_ "github.com/go-kivik/couchdb" // The CouchDB driver
	"github.com/go-kivik/kivik"     // Development version of Kivik
	// "github.com/gorilla/mux"



	//  "github.com/hyperledger/fabric/core/chaincode/lib/cid"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("StockChaincode")

// StockChaincode implementation
type StockChaincode struct {
	testMode bool
}

//Mapping the invoke functions
var bcFunctions = map[string]func(shim.ChaincodeStubInterface, []string) pb.Response{
	"asset-create":     CreateAsset,
	"asset-read":       ReadAsset,
	"asset-update": UpdateAsset,
	"asset-delete": DeleteAsset,
	"type-create": CreateType,
	"type-read":        ReadType,
	"type-update":      UpdateType,
	"type-delete":      DeleteType,
	"unit-create": CreateUnit,
	"unit-read":        ReadUnit,
	"unit-update":      UpdateUnit,
	"unit-delete":      DeleteUnit,
	"assettype-read":   ReadAssetType,
	"assettype-update": UpdateAssetType,
	"assettype-delete": DeleteAssetType,
	
	"assettype-create": CreateAssetType,
	"query":          Query,
	"statustype-create" : CreateStatusType,
	"statustype-delete" : DeleteStatusType,
	
	"statustype-list" : GetStatusType,
	"get-asset": GetAsset,
	"get-request": GetRequest,
	"get-assettype": GetAssetType,
	"request-create": CreateRequest,
	"request-read": ReadRequest,
	"requeststatus-update": UpdateRequestStatus,
	"requestqty-update": UpdateRequestQty,
	"request-delete": DeleteRequest,
	"asset-transfer": TransferAsset,
	"get-count":GetCount,
	"asset-qty":AssetQuantityPerOwner,
	"assetqty-assettype":AssetQuantityPerAssetType,
	"assettransfer-query":AssetTransferQuery,
	"assettype-unit": AssetTypeUnit,

	
}

//Init implementation for initialising the chaincode
func (t *StockChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {

	var err error
	fmt.Println("Initializing Stock Management")
	_, args := stub.GetFunctionAndParameters()

	if len(args) != 0 {
		err = fmt.Errorf("No arguments expected but found %d", len(args))
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

// Invoke Function accept blockchain code invocations.
func (t *StockChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	//  msp, err := cid.GetMSPID(stub)

	//  if err != nil {
	// 	 return shim.Error(err.Error())
	//  }
	//  if msp != "mohaMSP" && msp != "ecMSP" {
	// 	 return shim.Error("You don't have certificate from valid MSP")
	//  }

	//  certificate, err := cid.GetX509Certificate(stub)
	//  if err != nil {
	// 	 return shim.Error(err.Error())
	//  }

	//  if certificate.Issuer.CommonName != "ca.moha.nid.com" && certificate.Issuer.CommonName != "ca.ec.nid.com" {
	// 	 return shim.Error("Incorrect certificate issuer name")
	//  }

	function, args := stub.GetFunctionAndParameters()

	if function == "init" {
		return t.Init(stub)
	}
	bcFunc := bcFunctions[function]
	if bcFunc == nil {
		return shim.Error("Invalid invoke function.")
	}
	return bcFunc(stub, args)
}


//db1,db2,db3 database
var db1,db2,db3 *kivik.DB
//flow variable
var flow =map[string]*kivik.DB{}


func main() {

	
	client, err  := kivik.New("couch", "http://103.233.56.101:5035")
	if err != nil {
		panic(err)
	}

	db1 = client.DB(context.TODO(), "users")
	flow["ep"]=db1

	db2 = client.DB(context.TODO(), "user")

	db3 = client.DB(context.TODO(), "userss")
	flow["ad"]=db2
	flow["od"]=db3
	if err != nil {
		panic(err)
	}


	
	
	logger.SetLevel(shim.LogInfo)

	sc := new(StockChaincode)
	InitMap()

	err = shim.Start(sc)
	if err != nil {
		fmt.Printf("Error starting Stock chaincode: %s", err)
	}
}
