package main

//Transfer structure
type Transfer struct{
	DocType string `json:"docType"`
	From string `json:"from"`
	To string `json:"to"`
	AssetName string `json:"assetName"`
	AssetType string `json:"assetType"`
	Qty interface{} `json:"qty"`
	TransferDate int64 `json:"transferDate"`

}

//Attrs structure
type Attrs struct{
	Name string `json:"name"`
	Type string `json:"type"`
	Mandatory bool `json:"mandatory"`
}

//Type struct
type Type struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	
}
//Unit struct
type Unit struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	Type string `json:"type"`
}

//AssetType structure
type AssetType struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	Attributes []Attrs `json:"attributes"`
	Unit string `json:"unit"`

}

//NameValue structure
type NameValue struct{
	Name string `json:"name"`
	Value interface{} `json:"value"`
}

//Asset structure
type Asset struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	AssetType string `json:"assetType"`
	Attributes []NameValue `json:"attributes"`
	Qty interface{} `json:"qty"`
	Price float64 `json:"price"`
	Username string `json:"username"`
	Orgname string `json:"orgname"`
	Show bool `json:"show"`
	

}

//Request structure
type Request struct {
	DocType   string `json:"docType"`

	Qty       interface{}    `json:"qty"`
	
	Username string `json:"username"`
	Orgname string `json:"orgname"`
	Status    string `json:"status"`
	RequestedDate int    `json:"requestedDate"`

	AssetKey  string `json:"assetKey"`
	
}
//statusType structure
type statusType struct {
	DocType    string `json:"docType"`
	StatusType string `json:"statusType"`

}

//Count structure
type Count struct{
	Count map[string]int `json:"count"`
}

