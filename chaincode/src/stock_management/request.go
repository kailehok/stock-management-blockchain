package main

import (
	 "bytes"
	"encoding/json"
	"fmt"
	"time"

	// "os"
	"strings"

	"github.com/google/uuid"

	validator "gopkg.in/validator.v2"
	"math"



	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//CreateRequest creates request
func CreateRequest(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// // //Check for attribute permission
	// if true {

	// 	//Check for organization permission
	// 	if ok, _ := authenticateEmployeeEntityOrg(stub); !ok {
	// 		return shim.Error("Permission Denied")
	// 	}
	// }
	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`

	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_CREATE_REQUEST")
	if !check{
		shim.Error("Permission Denied!")
	}

	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count\n")
	}

	
	fmt.Println("*******")
	fmt.Println(info.Orgname)
	fmt.Println("*******")

	rqt := Request{}
	rqt.Status = "WAITING"
	rqt.DocType = "Request"

	ast := Asset{}

	err = json.Unmarshal([]byte(args[0]), &rqt)
	if err != nil {
		return shim.Error(err.Error())
	}

	//go validation
	if errs := validator.Validate(rqt); errs != nil {
		return shim.Error(errs.Error())
	}

	val, err := stub.GetState(rqt.AssetKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("No asset found.")
	}

	err = json.Unmarshal(val, &ast)
	if err != nil {
		return shim.Error(err.Error())
	}

	if ast.Orgname==info.Orgname{
		err := fmt.Sprintf("%s can't access the %s's assets", info.Username,ast.Username)
		return shim.Error(err)
	

	}
	//*********
	//need to check for the type(int or float) of qty in request using asset data
	//********

	if rqt.Qty.(float64) > ast.Qty.(float64) {
		return shim.Error("Requested Quantity is greater than the available quantity. ")
	}

	//Check if correct status type is provided
	statusTypeKey, err := stub.CreateCompositeKey("StatusType", []string{rqt.Status})
	if err != nil {
		return shim.Error(err.Error())
	}

	val, err = stub.GetState(statusTypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("Invalid status type \n")
	}

	//Marshal the data and put into the ledger
	// key for the ledger
	id, err := uuid.NewUUID()
	if err != nil {
		// handle error
	}
	requestKey, err := stub.CreateCompositeKey("Request", []string{id.String(), rqt.Username})
	if err != nil {
		return shim.Error(err.Error())
	}
	//value for the ledger
	requestAsBytes, err := json.Marshal(rqt)
	if err != nil {
		return shim.Error("Error marshaling request structure")
	}
	//Put the key value pair in ledger
	err = stub.PutState(requestKey, requestAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(requestAsBytes)
}

//DeleteRequest updates request
func DeleteRequest(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// // //Check for attribute permission
	// if true {

	// 	//Check for organization permission
	// 	if ok, _ := authenticateEmployeeEntityOrg(stub); !ok {
	// 		return shim.Error("Permission Denied")
	// 	}
	// }

	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
RequestKey string `json:"requestKey"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_DELETE_REQUEST")
	if !check{
		shim.Error("Permission Denied!")
	}

	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count\n")
	}

	rqt := Request{}



	val, err := stub.GetState(info.RequestKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("No request found.")
	}

	err = json.Unmarshal(val, &rqt)
	if err != nil {
		return shim.Error(err.Error())
	}

	//go validation
	if errs := validator.Validate(rqt); errs != nil {
		return shim.Error(errs.Error())
	}
	if strings.ToLower(rqt.Status) == "delivered" {
		return shim.Error("Delivered request cannot be deleted")
	}
	if rqt.Username != info.Username {
		err := fmt.Sprintf("%s doesnt have access to delete %s's request", info.Username, rqt.Username)

		return shim.Error(err)

	}
	//Put the key value pair in ledger
	err = stub.DelState(info.RequestKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

// //UpdateAsset updates the asset
// func UpdateAsset(stub shim.ChaincodeStubInterface, args []string) pb.Response {

// 	//Check for attribute permission
// 	if false {
// 		err := cid.AssertAttributeValue(stub, "CAN_UPDATE_ASSET", "true")
// 		if err != nil {
// 			return shim.Error(err.Error())
// 		}
// 	}

// 	//Check for correct number of arguments
// 	if len(args) != 1 {
// 		return shim.Error("Invalid argument count\n")
// 	}

// 	// Take the required structs from the argument
// 	partial := struct {
// 		AssetKey string `json:"assetKey"`
// 		Qty      int    `json:"qty"`
// 		Owner    string `json:"owner"`
// 	}{}

// 	errf := json.Unmarshal([]byte(args[0]), &partial)
// 	if errf != nil {
// 		return shim.Error(errf.Error())
// 	}

// 	ast := asset{}

// 	val, err := stub.GetState(partial.AssetKey)
// 	if err != nil {
// 		return shim.Error(err.Error())
// 	}

// 	if val == nil {
// 		return shim.Error("No asset found.")
// 	}

// 	err = json.Unmarshal(val, &ast)

// 	if err != nil {
// 		return shim.Error(err.Error())
// 	}

// 	//go validation
// 	if errs := validator.Validate(ast); errs != nil {
// 		return shim.Error(errs.Error())
// 	}

// 	if ast.Owner != partial.Owner {
// 		err := fmt.Sprintf("%s doesnt have access to update %s's asset", partial.Owner, ast.Owner)

// 		return shim.Error(err)
// 	}

// 	// errp := json.Unmarshal([]byte(args[0]), &partial)
// 	// if errp != nil {
// 	// 	return shim.Error(errp.Error())
// 	// }

// 	ast.Qty = partial.Qty

// 	//value for the ledger
// 	assetAsBytes, err := json.Marshal(ast)
// 	if err != nil {
// 		return shim.Error("Error marshaling province structure")
// 	}
// 	//Put the key value pair in ledger
// 	err = stub.PutState(partial.AssetKey, assetAsBytes)
// 	if err != nil {
// 		return shim.Error(err.Error())
// 	}

// 	return shim.Success(assetAsBytes)
// }

//UpdateRequestStatus updates request
func UpdateRequestStatus(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// // //Check for attribute permission
	// if true {

	// 	//Check for organization permission
	// 	if ok, _ := authenticateOperationsOrg(stub); !ok {
	// 		return shim.Error("Permission Denied")
	// 	}
	// }

	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	RequestKey string `json:"requestKey"`
	Qty interface{} `json:"qty"`
	Status     string `json:"status"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_UPDATE_REQUESTSTATUS")
	if !check{
		shim.Error("Permission Denied!")
	}
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count\n")
	}

	rqt := Request{}

	

	val, err := stub.GetState(info.RequestKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("No request found.")
	}

	err = json.Unmarshal(val, &rqt)
	if err != nil {
		return shim.Error(err.Error())
	}
	//go validation
	if errs := validator.Validate(rqt); errs != nil {
		return shim.Error(errs.Error())
	}
	if strings.ToLower(rqt.Status) == "delivered" {
		return shim.Error("Delivered request cannot be updated")
	}
	if strings.ToLower(info.Status) == "delivered" {
		return shim.Error("Permission denied")
	}

	rqt.Status = info.Status
	if info.Qty.(float64)>rqt.Qty.(float64){

		err := fmt.Sprintf("Approved quantity %f is greater than the requested quantity %f ", info.Qty.(float64), rqt.Qty.(float64))

		return shim.Error(err)

	}
	rqt.Qty= info.Qty
	//value for the ledger
	requestAsBytes, err := json.Marshal(rqt)
	if err != nil {
		return shim.Error("Error marshaling request structure")
	}

	//Put the key value pair in ledger
	err = stub.PutState(info.RequestKey, requestAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(requestAsBytes)
}

//UpdateRequestQty updates request
func UpdateRequestQty(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//Check for attribute permission




	// if true{

	// 	// Check for organization permission
	// 	if ok, _ := authenticateEmployeeEntityOrg(stub); !ok {
	// 		return shim.Error("Permission Denied")
	// 	}
	// }


	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	RequestKey string `json:"requestKey"`
	Qty interface{} `json:"qty"`
	
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_UPDATE_REQUESTQTY")
	if !check{
		shim.Error("Permission Denied!")
	}


	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count\n")
	}

	rqt := Request{}



	


	if info.Qty.(float64)==0{
	  return shim.Error("Quantity cant be zero. Delete the request!")
	}



	val, err := stub.GetState(info.RequestKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("No request found.")
	}

	err = json.Unmarshal(val, &rqt)
	if err != nil {
		return shim.Error(err.Error())
	}
	if strings.ToLower(rqt.Status) == "delivered" {
		return shim.Error("Delivered request cannot be updated")
	}
	if strings.ToLower(rqt.Status) != "waiting" {
		return shim.Error("Sorry your request has already been processed!")
	}


	if rqt.Username != info.Username {
		err := fmt.Sprintf("%s doesnt have access to update %s's request", info.Username, rqt.Username)

		return shim.Error(err)
	}

	//go validation
	if errs := validator.Validate(rqt); errs != nil {
		return shim.Error(errs.Error())
	}

	rqt.Qty = info.Qty

	val, err = stub.GetState(rqt.AssetKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("No asset found.")
	}

	ast := Asset{}
	err = json.Unmarshal(val, &ast)
	if err != nil {
		return shim.Error(err.Error())
	}

	if rqt.Qty.(float64) > ast.Qty.(float64) {
		return shim.Error("Requested Quantity is greater than the available quantity. ")
	}

	//**********

	//Create assetType key
	assetTypeKey, err := stub.CreateCompositeKey("AssetType", []string{ast.AssetType})
	if err != nil {
		return shim.Error(err.Error())
	}
	//Check if data exists for this key
	val, err = stub.GetState(assetTypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	unit := struct {
		Unit string `json:"unit"`
	}{}

	err = json.Unmarshal(val, &unit)

	unitKey, err := stub.CreateCompositeKey("Unit", []string{unit.Unit})
	if err != nil {
		return shim.Error(err.Error())
	}
	//Check if data exists for this key
	val, err = stub.GetState(unitKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	test := struct {
		Type string `json:"type"`
	}{}

	err = json.Unmarshal(val, &test)
	if err != nil {
		return shim.Error(err.Error())
	}
	if test.Type == "int" {
		if float64(rqt.Qty.(float64)) != math.Floor(rqt.Qty.(float64)) {
			err := fmt.Sprintf("Please provide integer value")
			return shim.Error(err)

		}
		rqt.Qty = int(rqt.Qty.(float64))
	}
	if test.Type == "float" {
		rqt.Qty = rqt.Qty.(float64)
	}

	//*********
	//value for the ledger
	requestAsBytes, err := json.Marshal(rqt)
	if err != nil {
		return shim.Error("Error marshaling request structure")
	}

	//Put the key value pair in ledger
	err = stub.PutState(info.RequestKey, requestAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(requestAsBytes)
}

//TransferAsset structure
func TransferAsset(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// //Check for attribute permission
	// if true {

	// 	//Check for organization permission
	// 	if ok, _ := authenticateAdministrationOrg(stub); !ok {
	// 		return shim.Error("Permission Denied")
	// 	}
	// 	}


	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	RequestKey string `json:"requestKey"`

	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_UPDATE_REQUESTSTATUS")
	if !check{
		shim.Error("Permission Denied!")
	}

	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count\n")
	}

	ast := Asset{}
	rqt := Request{}


	// requestKey, err := stub.CreateCompositeKey("Request", []string{partial.RequestUUID, partial.Requestor})
	// if err != nil {
	// 	return shim.Error(err.Error())
	// }

	// assetKey, err := stub.CreateCompositeKey("Asset", []string{partial.AssetUUID, owner})
	// if err != nil {
	// 	return shim.Error(err.Error())
	// }

	//Getting request info
	val, err := stub.GetState(info.RequestKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("No request found.")
	}

	err = json.Unmarshal(val, &rqt)
	if err != nil {
		return shim.Error(err.Error())
	}

	//Getting asset info
	val, err = stub.GetState(rqt.AssetKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("No asset found.")
	}

	err = json.Unmarshal(val, &ast)
	if err != nil {
		return shim.Error(err.Error())
	}

	if ast.Username != info.Username {
		err := fmt.Sprintf("%s doesnt have access to transfer %s's asset", info.Username, ast.Username)

		return shim.Error(err)
	}

	//Checking for approval of request
	if rqt.Status != "APPROVED" {
		return shim.Error("The request must be approved first.")
	}

	a := ast.Qty.(float64)
	b := rqt.Qty.(float64)
	//Carrying out transfer
	ast.Qty = a - b

	//Create assetType key
	assetTypeKey, err := stub.CreateCompositeKey("AssetType", []string{ast.AssetType})
	if err != nil {
		return shim.Error(err.Error())
	}
	//Check if data exists for this key
	val, err = stub.GetState(assetTypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	unit := struct {
		Unit string `json:"unit"`
	}{}

	err = json.Unmarshal(val, &unit)

	unitKey, err := stub.CreateCompositeKey("Unit", []string{unit.Unit})
	if err != nil {
		return shim.Error(err.Error())
	}
	//Check if data exists for this key
	val, err = stub.GetState(unitKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	test := struct {
		Type string `json:"type"`
	}{}

	err = json.Unmarshal(val, &test)
	if err != nil {
		return shim.Error(err.Error())
	}

	if test.Type == "int" {
		ast.Qty = int(ast.Qty.(float64))
	}
	if test.Type == "float" {
		ast.Qty = ast.Qty.(float64)
	}

	assetAsBytes, err := json.Marshal(ast)
	if err != nil {
		return shim.Error("Error marshaling asset structure")
	}
	err = stub.PutState(rqt.AssetKey, assetAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	id, err := uuid.NewUUID()
	if err != nil {
		// handle error
	}

	assetKey1, err := stub.CreateCompositeKey("Asset", []string{id.String(), rqt.Username})
	if err != nil {
		return shim.Error(err.Error())
	}

	ast1 := Asset{ast.DocType, ast.Name, ast.AssetType, ast.Attributes, rqt.Qty, ast.Price, rqt.Username,rqt.Orgname,ast.Show}
	//value for the ledger
	assetAsBytes, err = json.Marshal(ast1)
	if err != nil {
		return shim.Error("Error marshaling asset structure")
	}
	//Put the key value pair in ledger
	err = stub.PutState(assetKey1, assetAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	//For transfer document
	id, err = uuid.NewUUID()
	if err != nil {
		// handle error
	}

	trasnferKey, err := stub.CreateCompositeKey("Transfer", []string{id.String(), ast.Name})
	transferAst:=Transfer{"Transfer",ast.Username,rqt.Username,ast.Name,ast.AssetType,rqt.Qty,time.Now().Unix()*1000}
	if err != nil {
		return shim.Error(err.Error())
	}

	transferAsBytes, err := json.Marshal(transferAst)
	if err != nil {
		return shim.Error("Error marshaling asset structure")
	}
		//Put the key value pair in ledger
		err = stub.PutState(trasnferKey, transferAsBytes)
		if err != nil {
			return shim.Error(err.Error())
		}


	rqt.Status = "DELIVERED"
	requestAsBytes, err := json.Marshal(rqt)
	if err != nil {
		return shim.Error("Error marshaling request structure")
	}
	//Put the key value pair in ledger
	err = stub.PutState(info.RequestKey, requestAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

//ReadRequest returns all requests
func ReadRequest(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//Check for attribute permission

	
	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_UPDATE_REQUESTSTATUS")
	if !check{
		shim.Error("Permission Denied!")
	}


	if len(args) != 1 {
		return shim.Error("Expected no arguments")
	}

	resultsIterator, err := stub.GetStateByPartialCompositeKey("Request", []string{})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	results := []interface{}{}
	for resultsIterator.HasNext() {
		kvResult, _ := resultsIterator.Next()
		// if err != nil {
		// 	return shim.Error(err.Error())
		// }

		//Construct response struct
		result := struct {
			RequestKey string `json:"requestKey"`
			DocType    string `json:"docType"`

			Qty interface{} `json:"qty" validate:"nonzero"`

			Username     string `json:"username"`
			Orgname string `json:"orgname"`
			Status        string `json:"status"`
			RequestedDate int    `json:"requestedDate"`
			QtyInStock  interface{} `json:"qtyInStock"`
			AssetName    string `json:"assetName"`
			AssetKey      string `json:"assetKey"`
		}{}

		

		err = json.Unmarshal(kvResult.Value, &result)
		if err != nil {
			return shim.Error(err.Error())
		}
		if strings.ToLower(result.Status) == "delivered" {
			continue
		}

		if info.Orgname=="ep" && info.Username!=result.Username{
			continue
		}
		val,err:=stub.GetState(result.AssetKey)
		if err!=nil{
			return shim.Error(err.Error())
		}

		var ast Asset

		err=json.Unmarshal(val,&ast)
		result.AssetName=ast.Name
		result.QtyInStock=ast.Qty


		

		//Fetch Key
		result.RequestKey = kvResult.Key
		results = append(results, result)
	}
	resultsAsBytes, err := json.Marshal(results)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultsAsBytes)
}

//Query takes a couch db query syntax and returns the result
func Query(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//Check for attribute permission
	// if false {
	// 	err := cid.AssertAttributeValue(stub, "CAN_QUERY", "true")
	// 	if err != nil {
	// 		return shim.Error(err.Error())
	// 	}
	// }
	// "queryString"
	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	queryString := args[0]

	queryResults, err := getQueryResultForQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

// =========================================================================================
// getQueryResultForQueryString executes the passed in query string.
// Result set is built and returned as a byte array containing the JSON results.
// =========================================================================================
func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryRecords
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		queryResponse.Key = strings.Replace(queryResponse.Key, "\u0000", "\\u0000", -1)
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.Bytes(), nil
}


