package main

import (
	"encoding/json"
	// "fmt"
	// "reflect"

	// "github.com/google/uuid"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//GetAsset returns all asset
func GetAsset(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//Check for attribute permission
	// if false {
	// 	err := cid.AssertAttributeValue(stub, "CAN_READ_ASSET", "true")
	// 	if err != nil {
	// 		return shim.Error(err.Error())
	// 	}
	// }

	if len(args) != 1 {
		return shim.Error("Invalid argument count")
	}

	partial := struct {
		AssetKey string `json:"assetKey"`
	}{}

	err := json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		shim.Error("Error in marshalling")
	}

	val, err := stub.GetState(partial.AssetKey)
	if err != nil {
		shim.Error(err.Error())
	}
	if val == nil {
		shim.Error("No such asset found")
	}

	result := struct {
	
		AssetKey   string      `json:"assetKey"`
		DocType    string      `json:"docType"`
		Name       string      `json:"name"`
		AssetType  string      `json:"assetType"`
		Attributes []NameValue `json:"attributes"`
		Qty        interface{} `json:"qty"`
		Price      float64     `json:"price"`
		Owner      string      `json:"owner"`
		OwnerOrg string `json:"ownerOrg"`
		Show bool `json:"show"`

	}{}
	err = json.Unmarshal(val, &result)
	if err != nil {
		shim.Error("Error in unmarshalling")
	}
	result.AssetKey = partial.AssetKey

	resultsAsBytes, err := json.Marshal(result)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultsAsBytes)
}

//GetRequest returns all asset
func GetRequest(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//Check for attribute permission
	// if false {
	// 	err := cid.AssertAttributeValue(stub, "CAN_READ_ASSET", "true")
	// 	if err != nil {
	// 		return shim.Error(err.Error())
	// 	}
	// }

	if len(args) != 1 {
		return shim.Error("Invalid argument count")
	}

	partial := struct {
		RequestKey string `json:"requestKey"`
	}{}

	err := json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		shim.Error("Error in marshalling")
	}

	val, err := stub.GetState(partial.RequestKey)
	if err != nil {
		shim.Error(err.Error())
	}
	if val == nil {
		shim.Error("No such asset found")
	}

	result := struct {
		RequestKey string `json:"requestKey"`
		DocType  string `json:"docType"`

		Qty interface{} `json:"qty" validate:"nonzero"`

		Requestor     string `json:"requestor"`
		RequestorOrg  string `json:"requestorOrg"`
		Status        string `json:"status"`
		RequestedDate int    `json:"requestedDate"`
		AssetKey      string `json:"assetKey"`
	}{}
	err = json.Unmarshal(val, &result)
	if err != nil {
		shim.Error("Error in unmarshalling")
	}
	result.RequestKey = partial.RequestKey

	resultsAsBytes, err := json.Marshal(result)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultsAsBytes)
}

// GetAssetType returns all asset
func GetAssetType(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//Check for attribute permission
	// if false {
	// 	err := cid.AssertAttributeValue(stub, "CAN_READ_ASSET", "true")
	// 	if err != nil {
	// 		return shim.Error(err.Error())
	// 	}
	// }

	if len(args) != 1 {
		return shim.Error("Invalid argument count")
	}

	partial := struct {
		AssetTypeKey string `json:"assetTypeKey"`
	}{}

	err := json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		shim.Error("Error in marshalling")
	}

	val, err := stub.GetState(partial.AssetTypeKey)
	if err != nil {
		shim.Error(err.Error())
	}
	if val == nil {
		shim.Error("No such asset found")
	}

	result := struct {
		AssetTypeKey string `json:"requestKey"`
		DocType string `json:"docType"`
	Name string `json:"name"`
	Attributes []Attrs `json:"attributes"`
	Unit  string `json:"unit"`
	}{}
	err = json.Unmarshal(val, &result)
	if err != nil {
		shim.Error("Error in unmarshalling")
	}
	result.AssetTypeKey = partial.AssetTypeKey

	resultsAsBytes, err := json.Marshal(result)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultsAsBytes)
}

//GetCount function
func GetCount( stub shim.ChaincodeStubInterface,args []string) pb.Response{


	//Check for attribute permission
	// if false {
	// 	err := cid.AssertAttributeValue(stub, "CAN_READ_ASSET", "true")
	// 	if err != nil {
	// 		return shim.Error(err.Error())
	// 	}
	// }

	if len(args) != 0 {
		return shim.Error("Invalid argument count")
	}



	val, err := stub.GetState("Count")
	if err != nil {
		shim.Error(err.Error())
	}
	if val == nil {
		shim.Error("No such asset found")
	}

	var result Count
	err = json.Unmarshal(val, &result)
	if err != nil {
		shim.Error("Error in unmarshalling")
	}
	

	resultAsBytes, err := json.Marshal(result)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultAsBytes)
}

