package main

import (
	"encoding/json"
	"fmt"
	"testing"
	"github.com/google/uuid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
    "github.com/stretchr/testify/assert"
)	



func TestInvokeFunction(t *testing.T) {
	fmt.Println("Executing ijjnvoke functions")
	InitMap()

	// testdata  := []asset{}
	assert := assert.New(t)

	// Instantiate mockStub using AidChaincode as the target chaincode to unit test
	stub := shim.NewMockStub("TestStub", new(StockChaincode))
	assert.NotNil(stub, "Stub is nil, Test stub creation failed")
// Refresh Available funds under Project
	// result1 := stub.MockInvoke(uid,
	// 	[][]byte{[]byte("asset-read")})

	// fmt.Println(string(result1.GetPayload()))

	// assert.EqualValues(shim.OK, result1.GetStatus(), " failed to read the project data")
	uid:=uuid.New().String()

	typeOf := Type{"Type","date"}
	// ast1 := asset{"Asset","Lptop", "This is a Leno Laptop", "n-perishable", 5, 90000, 11111,111111, "ajaya"}

	typeAsBytes, _ := json.Marshal(typeOf)
	
	result := stub.MockInvoke(uid,[][]byte{[]byte("type-create"), typeAsBytes})
	typeOf = Type{"Type","float"}
	fmt.Println(result)


	typeAsBytes, _ = json.Marshal(typeOf)

	result= stub.MockInvoke(uid,[][]byte{[]byte("type-create"), typeAsBytes})
	typeOf = Type{"Type","bool"}


	typeAsBytes, _ = json.Marshal(typeOf)

	result= stub.MockInvoke(uid,[][]byte{[]byte("type-create"), typeAsBytes})
	typeOf = Type{"Type","int"}


	typeAsBytes, _ = json.Marshal(typeOf)

	result= stub.MockInvoke(uid,[][]byte{[]byte("type-create"), typeAsBytes})
	typeOf = Type{"Type","string"}


	typeAsBytes, _ = json.Marshal(typeOf)

	result= stub.MockInvoke(uid,[][]byte{[]byte("type-create"), typeAsBytes})
	fmt.Println("Type created")

	unit:=Unit{"Unit","kg","float"}
	unitAsBytes,_:=json.Marshal(unit)
	result= stub.MockInvoke(uid,[][]byte{[]byte("unit-create"), unitAsBytes})
	fmt.Println("Unit created")


	

	astType:=AssetType{"AssetType","Laptop",[]Attrs{{"Gen","int",true},{"SSD","float",true},{"RAM","float",false}},"kg"}
	astTypeAsBytes, _ := json.Marshal(astType)


	assetTypeResult := stub.MockInvoke(uid,[][]byte{[]byte("assettype-create"), astTypeAsBytes})
	fmt.Println(string(assetTypeResult.GetPayload()))
	assert.EqualValues(shim.OK, assetTypeResult.GetStatus(), "Saving test project to state failed."+assetTypeResult.GetMessage())

ast:=[]Asset{{"Asset","Acer","Laptop",[]NameValue{{"Gen",1234},{"SSD",5}},4,5555,"kailash","ad",true},{"Asset","Dell","Laptop",[]NameValue{{"Gen",1234},{"SSD",5}},4,5555,"kailash","ad",true}}
	
	astAsBytes, _ := json.Marshal(ast)
	astResult := stub.MockInvoke(uid,[][]byte{[]byte("asset-create"), astAsBytes})
	fmt.Println(string(astResult.GetPayload()))
	assert.EqualValues(shim.OK, astResult.GetStatus(), "Saving test project to state failed."+astResult.GetMessage())


	astResult = stub.MockInvoke(uid,[][]byte{[]byte("asset-qty")})
	fmt.Println(string(astResult.GetPayload()))
	assert.EqualValues(shim.OK, astResult.GetStatus(), "Saving test project to state failed."+astResult.GetMessage())

	astResult = stub.MockInvoke(uid,[][]byte{[]byte("assetqty-assettype")})
	fmt.Println(string(astResult.GetPayload()))
	assert.EqualValues(shim.OK, astResult.GetStatus(), "Saving test project to state failed."+astResult.GetMessage())



	astResult = stub.MockInvoke(uid,[][]byte{[]byte("assettransfer-query")})
	fmt.Println(string(astResult.GetPayload()))
	assert.EqualValues(shim.OK, astResult.GetStatus(), "Saving test project to state failed."+astResult.GetMessage())
}

// 	rqt:=request{"Request","Laptop",3,"kailash","APPROVED","jjj"}
// 	rqtAsBytes,_:=json.Marshal(rqt)

// 	result2 := stub.MockInvoke(uid,
// 		[][]byte{[]byte("request-create"), rqtAsBytes})

// 	fmt.Println(string(result2.GetPayload()))
	
// 	assert.EqualValues(shim.OK, result2.GetStatus(), "Saving test project to state failed."+result.GetMessage())

// 	// Refresh Available funds under Project
// 	result3 := stub.MockInvoke(uid,
// 		[][]byte{[]byte("request-read")})

// 	fmt.Println(string(result3.GetPayload()))

// 	assert.EqualValues(shim.OK, result3.GetStatus(), " failed to read the project data")

// 	transfer:= struct{
// 		Name string `json:"name"`
// 		Requestor string `json:"requestor"`

// 	}{"Laptop","kailash"}

// 	transferAsBytes,_:=json.Marshal(transfer)

// 	result4 := stub.MockInvoke(uid,
// 		[][]byte{[]byte("transfer-asset"), transferAsBytes})

// 	fmt.Println(string(result4.GetPayload()))
	
// 	assert.EqualValues(shim.OK, result4.GetStatus(), "Saving test project to state failed."+result.GetMessage())

// 	// Refresh Available funds under Project
// 	result1 = stub.MockInvoke(uid,
// 		[][]byte{[]byte("asset-read")})

// 	fmt.Println(string(result1.GetPayload()))

// 	assert.EqualValues(shim.OK, result1.GetStatus(), " failed to read the project data")

// 	// Refresh Available funds under Project
// 	result3 = stub.MockInvoke(uid,
// 		[][]byte{[]byte("request-read")})

// 	fmt.Println(string(result3.GetPayload()))

// 	assert.EqualValues(shim.OK, result3.GetStatus(), " failed to read the project data")


// fetchArgs:= struct {
// 		AssetKey string `json:"assetKey"`
// 		Qty int  `json:"qty"`
// 	}{"\u0000Asset\u0000Laptop\u0000ajaya\u0000",10}

// fetchArgsAsBytes,_:=json.Marshal(fetchArgs)

// result4 = stub.MockInvoke(uid,
// 	[][]byte{[]byte("update-asset"), fetchArgsAsBytes})

// fmt.Println(string(result4.GetPayload()))

// assert.EqualValues(shim.OK, result4.GetStatus(), "Saving test project to state failed."+result.GetMessage())

// result1 = stub.MockInvoke(uid,
// 	[][]byte{[]byte("asset-read")})

// fmt.Println(string(result1.GetPayload()))

// assert.EqualValues(shim.OK, result1.GetStatus(), " failed to read the project data")

	

// }
