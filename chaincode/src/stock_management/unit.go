package main

import (
	"encoding/json"
	"fmt"
	

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

//CreateUnit  creates Unit of the variable
func CreateUnit(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	UnitOf := Unit{}
	UnitOf.DocType = "Unit"

	err := json.Unmarshal([]byte(args[0]), &UnitOf)
	if err != nil {
		return shim.Error(err.Error())

	}
	if UnitOf.Type!="float" && UnitOf.Type!="int"{
		err := fmt.Sprintf("Type must be int or float")
     	return shim.Error(err)
	}

	//Create asset key
	UnitKey, err := stub.CreateCompositeKey("Unit", []string{UnitOf.Name})
	fmt.Println("dbsdjhvb")
	fmt.Println(UnitKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	//Check if data exists for this key
	val, err := stub.GetState(UnitKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val != nil {
		return shim.Error("Unit already exists")
	}

	//value as bytes to put in the ledger
	UnitAsBytes, err := json.Marshal(UnitOf)
	if err != nil {
		return shim.Error("Error marshalling the data")
	}

	//Put the key value in ledger
	err = stub.PutState(UnitKey, UnitAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println("******")
	fmt.Println(UnitKey,"\n",string(UnitAsBytes))
	fmt.Println("******")

	return shim.Success(UnitAsBytes)

}

//ReadUnit  creates Unit of the variable
func ReadUnit(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 0 {
		return shim.Error("Expected no arguments")
	}

	resultsIterator, err := stub.GetStateByPartialCompositeKey("Unit", []string{})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	results := []interface{}{}
	for resultsIterator.HasNext() {
		kvResult, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		//Construct response struct
		result := struct {
			UnitKey string `json:"UnitKey"`
			Name    string `json:"name"`
			Type string `json:"type"`
		}{}

		err = json.Unmarshal(kvResult.Value, &result)
		if err != nil {
			return shim.Error(err.Error())
		}

		//Fetch Key
		result.UnitKey = kvResult.Key
		results = append(results, result)
	}
	resultsAsBytes, err := json.Marshal(results)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultsAsBytes)
}

//UpdateUnit  creates Unit of the variable
func UpdateUnit(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	partial := struct {
		UnitKey string `json:"unitKey"`
		
	}{}

	unit:=Unit{}

	err := json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		return shim.Error(err.Error())

	}
	err = json.Unmarshal([]byte(args[0]), &unit)
	if err != nil {
		return shim.Error(err.Error())

	}

	//Check if data exists for this key
	val, err := stub.GetState(partial.UnitKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("Unit doesnt exists")
	}
	err=stub.DelState(partial.UnitKey)
	if err != nil {
		return shim.Error(err.Error())
	}
//Create asset key
UnitKey, err := stub.CreateCompositeKey("Unit", []string{unit.Name})

UnitAsBytes,err:=json.Marshal(unit)
if err!=nil{
	shim.Error(err.Error())
}



	//Put the key value in ledger
	err = stub.PutState(UnitKey, UnitAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(UnitAsBytes)

}

//DeleteUnit  creates Unit of the variable
func DeleteUnit(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	partial := struct {
		UnitKey string `json:"unitKey"`
	}{}

	err := json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		return shim.Error(err.Error())

	}

	//Check if data exists for this key
	val, err := stub.GetState(partial.UnitKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("Unit doesnt exists")
	}

	//Delete the key value from ledger
	err = stub.DelState(partial.UnitKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)

}
