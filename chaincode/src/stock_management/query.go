
package main

import (
	
	"encoding/json"
	"strconv"
	"fmt"
	"net/http"
	"io/ioutil"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"


	// "os"


)
//AssetQuantityPerOwner function
func AssetQuantityPerOwner(stub shim.ChaincodeStubInterface, args []string) pb.Response {
url := "http://103.233.56.101:5035/demo-channel_mycc/_design/query/_view/assettype?group_level=2"
 
req, _ := http.NewRequest("GET", url, nil)
 

 
res, err := http.DefaultClient.Do(req)
 
if err!=nil{
	fmt.Println(err.Error())
}
defer res.Body.Close()
body1, _ := ioutil.ReadAll(res.Body)

type Check struct{

	Key []string `json:"key"`
	Value string `json:"value"`
}
//Row struct
type Row struct{
Row []Check `json:"rows"`
}

var row Row 
err=json.Unmarshal(body1,&row)
 
fmt.Println(string(body1))
fmt.Println(row)
return shim.Success(body1)
}
//AssetQuantityPerAssetType function
func AssetQuantityPerAssetType(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	url := "http://103.233.56.101:5035/demo-channel_mycc/_design/query/_view/assettype?group_level=1"
	 
	req, _ := http.NewRequest("GET", url, nil)
	 
	
	 
	res, err := http.DefaultClient.Do(req)
	 
	if err!=nil{
		fmt.Println(err.Error())
	}
	defer res.Body.Close()
	body1, _ := ioutil.ReadAll(res.Body)
	
	type Check struct{
	
		Key []string `json:"key"`
		Value string `json:"value"`
	}
	//Row struct
	type Row struct{
	Row []Check `json:"rows"`
	}
	
	var row Row 
	err=json.Unmarshal(body1,&row)
	 
	fmt.Println(string(body1))
	fmt.Println(row)
	return shim.Success(body1)
	}


	//AssetTransferQuery function
func AssetTransferQuery(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	partial:=struct{
		StartKey int `json:"startKey"`
		EndKey int `json:"endKey"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&partial)
url := "http://103.233.56.101:5035/demo-channel_mycc/_design/query/_view/transfer?startkey=" +strconv.Itoa(partial.StartKey) +"&endkey="+strconv.Itoa(partial.EndKey)
	 
	req, _ := http.NewRequest("GET", url, nil)
	 
	
	 
	res, err := http.DefaultClient.Do(req)
	 
	if err!=nil{
		fmt.Println(err.Error())
	}
	defer res.Body.Close()
	body1, _ := ioutil.ReadAll(res.Body)
	
	type Check struct{
	
		Key []string `json:"key"`
		Value string `json:"value"`
	}
	//Row struct
	type Row struct{
	Row []Check `json:"rows"`
	}
	
	var row Row 
	err=json.Unmarshal(body1,&row)
	 
	fmt.Println(string(body1))
	fmt.Println(row)
	return shim.Success(body1)
	}



	//AssetTypeUnit function
func AssetTypeUnit(stub shim.ChaincodeStubInterface, args []string) pb.Response {
 



url := "http://103.233.56.101:5035/demo-channel_mycc/_design/query/_view/unit"
	 
	req, _ := http.NewRequest("GET", url, nil)
	 
	
	 
	res, err := http.DefaultClient.Do(req)
	 
	if err!=nil{
		fmt.Println(err.Error())
	}
	defer res.Body.Close()
	body1, _ := ioutil.ReadAll(res.Body)
	
	type Check struct{
	
		Key []string `json:"key"`
		Value string `json:"value"`
	}
	//Row struct
	type Row struct{
	Row []Check `json:"rows"`
	}
	
	var row Row 
	err=json.Unmarshal(body1,&row)
	 
	fmt.Println(string(body1))
	fmt.Println(row)
	return shim.Success(body1)
	}