package main

import (
	"encoding/json"
	"fmt"
	"reflect"
	
	"math"

	"github.com/google/uuid"
	// "github.com/hyperledger/fabric/core/chaincode/lib/cid"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)



 var count map[string]int
//InitMap function
 func InitMap(){
	 count=make(map[string]int)
	 fmt.Println(count)
 }

//CreateType  creates type of the variable
func CreateType(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	
	typeOf := Type{}
	typeOf.DocType = "Type"

	err := json.Unmarshal([]byte(args[0]), &typeOf)
	if err != nil {
		return shim.Error(err.Error())

	}

	//Create asset key
	typeKey, err := stub.CreateCompositeKey("Type", []string{typeOf.Name})
	if err != nil {
		return shim.Error(err.Error())
	}

	//Check if data exists for this key
	val, err := stub.GetState(typeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val != nil {
		return shim.Error("Type already exists")
	}

	//value as bytes to put in the ledger
	typeAsBytes, err := json.Marshal(typeOf)
	if err != nil {
		return shim.Error("Error marshalling the data")
	}

	//Put the key value in ledger
	err = stub.PutState(typeKey, typeAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println("******")
	fmt.Println(typeKey, "\n", string(typeAsBytes))
	fmt.Println("******")

	return shim.Success(typeAsBytes)

}

//ReadType  creates type of the variable
func ReadType(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 0 {
		return shim.Error("Expected no arguments")
	}

	resultsIterator, err := stub.GetStateByPartialCompositeKey("Type", []string{})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	results := []interface{}{}
	for resultsIterator.HasNext() {
		kvResult, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		//Construct response struct
		result := struct {
			TypeKey string `json:"typeKey"`
			Name    string `json:"name"`
		}{}

		err = json.Unmarshal(kvResult.Value, &result)
		if err != nil {
			return shim.Error(err.Error())
		}

		//Fetch Key
		result.TypeKey = kvResult.Key
		results = append(results, result)
	}
	resultsAsBytes, err := json.Marshal(results)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultsAsBytes)
}

//UpdateType  creates type of the variable
func UpdateType(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	partial := struct {
		TypeKey string `json:"typeKey"`
		Name    string `json:"type"`
	}{}

	err := json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		return shim.Error(err.Error())

	}

	//Check if data exists for this key
	val, err := stub.GetState(partial.TypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("Type doesnt exists")
	}

	typeOf := Type{}
	err = json.Unmarshal(val, typeOf)
	if err != nil {
		return shim.Error(err.Error())

	}
	typeOf.Name = partial.Name
	//value as bytes to put in the ledger
	typeAsBytes, err := json.Marshal(typeOf)
	if err != nil {
		return shim.Error("Error marshalling the data")
	}

	//Put the key value in ledger
	err = stub.PutState(partial.TypeKey, typeAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(typeAsBytes)

}

//DeleteType  creates type of the variable
func DeleteType(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	partial := struct {
		TypeKey string `json:"typeKey"`
	}{}

	err := json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		return shim.Error(err.Error())

	}

	//Check if data exists for this key
	val, err := stub.GetState(partial.TypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("Type doesnt exists")
	}

	//Delete the key value from ledger
	err = stub.DelState(partial.TypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)

}

//CreateAssetType creates the shape and line amount for an owner
func CreateAssetType(stub shim.ChaincodeStubInterface, args []string) pb.Response {


	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_CREATE_ASSETTYPE")
	if !check{
		shim.Error("Permission Denied!")
	}
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	astType := AssetType{}
	astType.DocType = "AssetType"

	err = json.Unmarshal([]byte(args[0]), &astType)
	if err != nil {
		return shim.Error(err.Error())
	}
	if astType.Unit==""{
		return shim.Error("Unit field is mandatory!")

	}

	a := make(map[string]int)
	var repeat []string
	//Check if valid shape is provided and only one entry per shape is provided
	for _, val := range astType.Attributes {

		if a[val.Name] == 1 {
			repeat = append(repeat, val.Name)

		} else {
			a[val.Name]++
		}

		typeKey, err := stub.CreateCompositeKey("Type", []string{val.Type})
		if err != nil {
			return shim.Error(err.Error())
		}
		val1, err := stub.GetState(typeKey)
		if val1 == nil {
			err := fmt.Sprintf("%s type is not defined", val.Type)
			return shim.Error(err)

		}

	}
	if len(repeat) != 0 {
		err := fmt.Sprintf("Duplicate fields %v", repeat)
		return shim.Error(err)

	}

	//Create assetType key
	assetTypeKey, err := stub.CreateCompositeKey("AssetType", []string{astType.Name})
	if err != nil {
		return shim.Error(err.Error())
	}

	//Check if data exists for this key
	val1, err := stub.GetState(assetTypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val1 != nil {
		return shim.Error("AssetType already exists")
	}

	//value as bytes to put in the ledger
	assetTypeAsBytes, err := json.Marshal(astType)
	if err != nil {
		return shim.Error("Error marshalling the data")
	}

	
	//Put the key value in ledger
	err = stub.PutState(assetTypeKey, assetTypeAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(assetTypeAsBytes)
}

//ReadAssetType  creates type of the variable
func ReadAssetType(stub shim.ChaincodeStubInterface, args []string) pb.Response {



	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_READ_ASSETTYPE")
	if !check{
		shim.Error("Permission Denied!")
	}
	if len(args) != 0 {
		return shim.Error("Expected no arguments")
	}

	resultsIterator, err := stub.GetStateByPartialCompositeKey("AssetType", []string{})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	results := []interface{}{}
	for resultsIterator.HasNext() {
		kvResult, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		//Construct response struct
		result := struct {
			AssetTypeKey string  `json:"assetTypeKey"`
			Name         string  `json:"name"`
			Attributes   []Attrs `json:"attributes"`
			Unit string `json:"unit"`
		}{}

		err = json.Unmarshal(kvResult.Value, &result)
		if err != nil {
			return shim.Error(err.Error())
		}

		//Fetch Key
		result.AssetTypeKey = kvResult.Key
		results = append(results, result)
	}
	resultsAsBytes, err := json.Marshal(results)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultsAsBytes)
}

//UpdateAssetType creates the shape and line amount for an owner
func UpdateAssetType(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_UPDATE_ASSETTYPE")
	if !check{
		shim.Error("Permission Denied!")
	}
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}
	partial := struct {
		AssetTypeKey string `json:"assetTypeKey"`
	}{}
	err = json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		return shim.Error(err.Error())
	}

	val, err := stub.GetState(partial.AssetTypeKey)
	if val == nil {
		err := fmt.Sprintf("%s assettype is not defined", partial.AssetTypeKey)
		return shim.Error(err)

	}
	//Delete the key value from ledger
	err = stub.DelState(partial.AssetTypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	astType := AssetType{}
	astType.DocType = "AssetType"

	err = json.Unmarshal([]byte(args[0]), &astType)
	if err != nil {
		return shim.Error(err.Error())
	}

	if astType.Unit==""{
		return shim.Error("Unit field is mandatory!")

	}

	// //Check if owner exists
	// _, err = get_owner(stub, shapeLineAmount.OwnerID)
	// if err != nil {
	// 	fmt.Println("Failed to find owner - " + shapeLineAmount.OwnerID)
	// 	return shim.Error(err.Error())
	// }
	a := make(map[string]int)
	var repeat []string
	//Check if valid shape is provided and only one entry per shape is provided
	for _, val := range astType.Attributes {

		if a[val.Name] == 1 {
			repeat = append(repeat, val.Name)

		} else {
			a[val.Name]++
		}

		typeKey, err := stub.CreateCompositeKey("Type", []string{val.Type})
		if err != nil {
			return shim.Error(err.Error())
		}
		val1, err := stub.GetState(typeKey)
		if val1 == nil {
			err := fmt.Sprintf("%s type is not defined", val.Type)
			return shim.Error(err)

		}

	}
	if len(repeat) != 0 {
		err := fmt.Sprintf("Duplicate fields %v", repeat)
		return shim.Error(err)

	}

	//Create assetType key
	assetTypeKey, err := stub.CreateCompositeKey("AssetType", []string{astType.Name})
	if err != nil {
		return shim.Error(err.Error())
	}

	//value as bytes to put in the ledger
	assetTypeAsBytes, err := json.Marshal(astType)
	if err != nil {
		return shim.Error("Error marshalling the data")
	}

	//Put the key value in ledger
	err = stub.PutState(assetTypeKey, assetTypeAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(assetTypeAsBytes)
}

//DeleteAssetType  creates type of the variable
func DeleteAssetType(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	
	
	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_DELETE_ASSETTYPE")
	if !check{
		shim.Error("Permission Denied!")
	}
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	partial := struct {
		AssetTypeKey string `json:"assetTypeKey"`
	}{}

	err = json.Unmarshal([]byte(args[0]), &partial)
	if err != nil {
		return shim.Error(err.Error())

	}

	//Check if data exists for this key
	val, err := stub.GetState(partial.AssetTypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("AssetType doesnt exists")
	}

	//Delete the key value from ledger
	err = stub.DelState(partial.AssetTypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)

}

//CreateAsset creates asset
func CreateAsset(stub shim.ChaincodeStubInterface, args []string) pb.Response {


	// if true {

	// 	//Check for organization permission
	// 	if ok, _ := authenticateAdministrationOrg(stub); !ok {
	// 		return shim.Error("Permission Denied")
	// 	}
	// }\

	

	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}

	var assetKey string
	var assetAsBytes []byte

	ast := []Asset{}



	err := json.Unmarshal([]byte(args[0]), &ast)
	if err != nil {
		return shim.Error(err.Error())
	}
	

	

	check:=checkPermission(ast[0].Username,ast[0].Orgname,"CAN_CREATE_ASSET")
	if !check{
		shim.Error("Permission Denied!")
	}
	



type key struct{
	Byte []byte 
	AssetType string
}

	keyValue := make(map[string]key)

	for _, a := range ast {
		a.DocType="Asset"

		//Create assetType key
		assetTypeKey, err := stub.CreateCompositeKey("AssetType", []string{a.AssetType})
		if err != nil {
			return shim.Error(err.Error())
		}
		//Check if data exists for this key
		val, err := stub.GetState(assetTypeKey)
		if err != nil {
			return shim.Error(err.Error())
		}
		if val == nil {
			err := fmt.Sprintf("%s assettype doesnt exist", a.AssetType)
			return shim.Error(err)
		}

		partial := struct {
			Attributes []Attrs `json:"attributes"`
			Unit       string  `json:"unit"`
		}{}

		err = json.Unmarshal(val, &partial)

	

		type Check struct {
			Type      string
			Mandatory bool
		}
		aMap := make(map[string]Check)

		for _, val := range partial.Attributes {
			aMap[val.Name] = Check{val.Type, val.Mandatory}

		}

		for i, val := range a.Attributes {
			aStruct := aMap[val.Name]
			if aStruct.Type == "" {
				err := fmt.Sprintf("%s attribute doesnt exist for assettype %s", val.Name, a.AssetType)
				return shim.Error(err)

			}
			b := fmt.Sprintf("%s", reflect.TypeOf(val.Value))
			c:=reflect.ValueOf(&val.Value).Elem()

			if b == "float64" {
				switch aMap[val.Name].Type {
				case "date":
					val.Value = int((c.Interface()).(float64))
					a.Attributes[i].Value = (val.Value).(int)
					b = "date"
				case "int":
					val.Value = int((c.Interface()).(float64))
					a.Attributes[i].Value = val.Value.(int)
					b = "int"
				default:
					val.Value = (c.Interface()).(float64)
					a.Attributes[i].Value = (val.Value).(float64)
					b = "float"


				}
			}
			fmt.Println(a.Attributes[i].Value)
			fmt.Printf("%T",a.Attributes[i].Value)
			if b != aMap[val.Name].Type {

				err := fmt.Sprintf("%s must be of type %s found %s", val.Name, aMap[val.Name].Type, b)
				return shim.Error(err)

			}

			copy := aMap[val.Name]
			copy.Mandatory = false
			aMap[val.Name] = copy

		}
		var mandatory []string
		for k, v := range aMap {
			if v.Mandatory == true {
				mandatory = append(mandatory, k)

			}
		}

		if len(mandatory) != 0 {
			err := fmt.Sprintf("Mandatory Fields %v", mandatory)
			return shim.Error(err)

		}

		//Create Unit key
		unitKey, err := stub.CreateCompositeKey("Unit", []string{partial.Unit})
		if err != nil {
			return shim.Error(err.Error())
		}
		//Check if data exists for this key
		val, err = stub.GetState(unitKey)
		if err != nil {
			return shim.Error(err.Error())
		}
		if val == nil {
			err := fmt.Sprintf("%s unit doesnt exist", partial.Unit)
			return shim.Error(err)
		}

		test := struct {
			Type string `json:"type"`
		}{}

		err = json.Unmarshal(val, &test)
		if err != nil {
			return shim.Error(err.Error())
		}
		if test.Type == "int" {
			if a.Qty.(float64)!=math.Floor(a.Qty.(float64)) {
				err := fmt.Sprintf("Expected int found float")
				return shim.Error(err)

			}
			a.Qty = int(a.Qty.(float64))
		}
		if test.Type == "float" {
			a.Qty = a.Qty.(float64)	
		}

		
		

		assetAsBytes, err = json.Marshal(a)
		if err != nil {
			return shim.Error(err.Error())
		}

		id, err := uuid.NewUUID()
		if err != nil {
			// handle error
		}
		//Create asset key
		assetKey, err = stub.CreateCompositeKey("Asset", []string{id.String(), a.Name})
		if err != nil {
			return shim.Error(err.Error())
		}

		keyValue[assetKey] = key{assetAsBytes,a.AssetType}
		// Put the key value in ledger
		// err = stub.PutState(assetKey, assetAsBytes)
		// if err != nil {
		// 	return shim.Error(err.Error())
		// }

		// fmt.Println("******")
		// fmt.Println(assetKey,"\n",string(assetAsBytes))
		// fmt.Println("******")

	}

	output := make(map[string]string)

	for k, v := range keyValue {
		//Put the key value in ledger
		err = stub.PutState(k, v.Byte)
		if err != nil {
			return shim.Error(err.Error())
		}
		count[v.AssetType]++
		countAsBytes, err := json.Marshal(count)
		if err != nil {

			return shim.Error(err.Error())
		}

		err=stub.PutState("Count",countAsBytes)
		if err != nil {
			return shim.Error(err.Error())
		}
		fmt.Println("******")
		fmt.Println(k, "\n", string(v.Byte))
		fmt.Println("******")
		output[k] = string(v.Byte)

	}
	keyValueAsBytes, err := json.Marshal(output)
	if err != nil {

		return shim.Error(err.Error())
	}

	return shim.Success(keyValueAsBytes)

}

//ReadAsset  creates type of the variable
func ReadAsset(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println(len(args))

	if len(args) != 1 {
		return shim.Error("Expected no arguments")
	}

	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_READ_ASSET")
	if !check{
		shim.Error("Permission Denied!")
	}

	// err:=json.Unmarshal([]byte(args[0]),&partial)

	resultsIterator, err := stub.GetStateByPartialCompositeKey("Asset", []string{})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	results := []interface{}{}
	for resultsIterator.HasNext() {
		kvResult, err := resultsIterator.Next()
		if err !=nil {
			fmt.Println("Panic")
		}
	

		//Construct response struct
		result := struct {
			AssetKey   string      `json:"assetKey"`
			DocType    string      `json:"docType"`
			Name       string      `json:"name"`
			AssetType  string      `json:"assetType"`
			Attributes []NameValue `json:"attributes"`
			Qty        interface{} `json:"qty"`
			Price      float64     `json:"price"`
			Username     string      `json:"username"`
			Orgname   string       `json:"orgname"`
			Show    bool `json:"show"`
		}{}

		err = json.Unmarshal(kvResult.Value, &result)
		if err != nil {
			return shim.Error(err.Error())
		}

		var check bool

		check,_=authenticateEmployeeEntityOrg(stub)
		if result.Qty== 0{
			continue
		}

		if result.Show==false && check {
			continue
		}
		if result.Orgname=="ep" && info.Orgname=="ep" && info.Username!=result.Username{
			continue
		}
		

		//Fetch Key
		result.AssetKey = kvResult.Key
		results = append(results, result)
	}
	resultsAsBytes, err := json.Marshal(results)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultsAsBytes)
}

//UpdateAsset creates asset
func UpdateAsset(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// if true {

	// 	//Check for organization permission
	// 	if ok, _ := authenticateAdministrationOrg(stub); !ok {
	// 		return shim.Error("Permission Denied")
	// 	}
	// }

	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	AssetKey string `json:"assetKey"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_UPDATE_ASSET")
	if !check{
		shim.Error("Permission Denied!")
	}
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}



	val, err := stub.GetState(info.AssetKey)
	if val == nil {
		err := fmt.Sprintf("No asset found for assetkey %s", info.AssetKey)
		return shim.Error(err)

	}
	//Delete the key value from ledger
	err = stub.DelState(info.AssetKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	ast := Asset{}
	ast.DocType = "Asset"
	err = json.Unmarshal([]byte(args[0]),&ast)
	if err != nil {
		return shim.Error(err.Error())
	}
	if ast.Username != info.Username {
		err := fmt.Sprintf("%s doesnt have access to update %s's asset", info.Username, ast.Username)

		return shim.Error(err)
	}
	//Create assetType key
	assetTypeKey, err := stub.CreateCompositeKey("AssetType", []string{ast.AssetType})
	if err != nil {
		return shim.Error(err.Error())
	}
	//Check if data exists for this key
	val, err = stub.GetState(assetTypeKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if val == nil {
		err := fmt.Sprintf("%s assettype doesnt exist", ast.AssetType)
		return shim.Error(err)
	}

	partial := struct {
		Attributes []Attrs `json:"attributes"`
		Unit string `json:"unit"`
	}{}

	err = json.Unmarshal(val, &partial)

	type Check struct {
		Type      string
		Mandatory bool
	}
	aMap := make(map[string]Check)

	for _, val := range partial.Attributes {
		aMap[val.Name] = Check{val.Type, val.Mandatory}

	}

	for i, val := range ast.Attributes {
		aStruct := aMap[val.Name]
		if aStruct.Type == "" {
			err := fmt.Sprintf("%s attribute doesnt exist for assettype %s", val.Name, ast.AssetType)
			return shim.Error(err)

		}
		b := fmt.Sprintf("%s", reflect.TypeOf(val.Value))

		if b == "float64" {
			switch aMap[val.Name].Type {
			case "date":
				val.Value = int((val.Value).(float64))
				ast.Attributes[i].Value = val.Value
				b = "date"
			case "int":
				val.Value = int((val.Value).(float64))
				ast.Attributes[i].Value = val.Value
				b = "int"
			default:

				b = "float"

			}
		}
		if b != aMap[val.Name].Type {

			err := fmt.Sprintf("%s must be of type %s found %s", val.Name, aMap[val.Name].Type, b)
			return shim.Error(err)

		}

		copy := aMap[val.Name]
		copy.Mandatory = false
		aMap[val.Name] = copy

	}
	var mandatory []string
	for k, v := range aMap {
		if v.Mandatory == true {
			mandatory = append(mandatory, k)

		}
	}

	if len(mandatory) != 0 {
		err := fmt.Sprintf("Mandatory Fields %v", mandatory)
		return shim.Error(err)

	}

	//Create Unit key
	unitKey, err := stub.CreateCompositeKey("Unit", []string{partial.Unit})
	if err != nil {
		return shim.Error(err.Error())
	}
	//Check if data exists for this key
	val, err = stub.GetState(unitKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if val == nil {
		err := fmt.Sprintf("%s unit doesnt exist", partial.Unit)
		return shim.Error(err)
	}

	test := struct {
		Type string `json:"type"`
	}{}

	err = json.Unmarshal(val, &test)
	if err != nil {
		return shim.Error(err.Error())
	}
	if test.Type == "int" {
		if ast.Qty.(float64)!=math.Floor(ast.Qty.(float64)) {
			err := fmt.Sprintf("Expected int found float")
			return shim.Error(err)

		}
		ast.Qty = int(ast.Qty.(float64))
	}
	if test.Type == "float" {
		ast.Qty = ast.Qty.(float64)	
	}

	assetAsBytes, err := json.Marshal(ast)
	if err != nil {
		return shim.Error(err.Error())
	}

	//Delete the key value from ledger
	err = stub.DelState(info.AssetKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	a := []rune(info.AssetKey)

    // Step 2: Grab the num of chars you need
    myShortString := string(a[7:43])

	//Create asset key
	assetKey, err := stub.CreateCompositeKey("Asset", []string{myShortString, ast.Name})
	if err != nil {
		return shim.Error(err.Error())
	}

	//Put the key value in ledger
	err = stub.PutState(assetKey, assetAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(assetAsBytes)
}

//DeleteAsset  creates type of the variable
func DeleteAsset(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// if true {

	// 	//Check for organization permission
	// 	if ok, _ := authenticateAdministrationOrg(stub); !ok {
	// 		return shim.Error("Permission Denied")
	// 	}
	// }

	info:=struct{
		Username string `json:"username"`
	Orgname string `json:"orgname"`
	AssetKey string `json:"assetKey"`
	}{}

	err:=json.Unmarshal([]byte(args[0]),&info)
	if err!=nil{
		return shim.Error(err.Error())
	}

	check:=checkPermission(info.Username,info.Orgname,"CAN_UPDATE_ASSET")
	if !check{
		shim.Error("Permission Denied!")
	}
	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count \n")
	}


	ast := Asset{}


	if err != nil {
		return shim.Error(err.Error())

	}

	//Check if data exists for this key
	val, err := stub.GetState(info.AssetKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("Asset doesnt exists")
	}
	err = json.Unmarshal(val, &ast)
	if err != nil {
		return shim.Error(err.Error())
	}
	if ast.Username != info.Username {
		err := fmt.Sprintf("%s doesnt have access to delete %s's asset", info.Username, ast.Username)

		return shim.Error(err)
	}

	//Delete the key value from ledger
	err = stub.DelState(info.AssetKey)
	if err != nil {
		return shim.Error(err.Error())

	}

	count[ast.AssetType]--
	countAsBytes, err := json.Marshal(count)
	if err != nil {

		return shim.Error(err.Error())
	}

	err=stub.PutState("Count",countAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)

}
