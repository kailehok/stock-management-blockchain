
package main

import (
	"encoding/json"
	// "bytes"
	// "fmt"
	// "reflect"

	// "github.com/google/uuid"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)
//CreateStatusType allows to create status for request
func CreateStatusType(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// //Check for attribute permission
	// err := cid.AssertAttributeValue(stub,"CAN_CREATE_STATUSTYPE","true")
	// if err != nil {
	// 	return shim.Error(err.Error())
	// }

	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count\n")
	}

	// //Take the required structs from the argument
	// partial := struct {
	// 	StatusType string `json:"statusType"`
	// }{}
	stsType:=statusType{}
	errp := json.Unmarshal([]byte(args[0]), &stsType)
	if errp != nil {
		return shim.Error(errp.Error())
	}
	stsType.DocType="StatusType"

	if stsType.StatusType == "" {
		return shim.Error("Didnt get any value for statusType")
	}

	//Marshal the data and put into the ledger
	// key for the ledger
	statusTypeKey, err := stub.CreateCompositeKey("StatusType", []string{stsType.StatusType})
	if err != nil {
		return shim.Error(err.Error())
	}
	//value for the ledger
	statusAsBytes, err := json.Marshal(stsType)
	if err != nil {
		return shim.Error("Error marshaling province structure")
	}
	//Put the key value pair in ledger
	err = stub.PutState(statusTypeKey, statusAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(statusAsBytes)

}




//UpdateStatusType enables to create the values for asset type
func UpdateStatusType(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//Check for attribute permission
	// err := cid.AssertAttributeValue(stub, "CAN_CREATE_ASSETTYPE", "true")
	// if err != nil {
	// 	return shim.Error(err.Error())
	// }

	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count\n")
	}
	partial:= struct{
		StatusTypeKey string `json:"statusTypeKey"`
	
	}{}
	stsType:=statusType{}


	errp := json.Unmarshal([]byte(args[0]), &partial)
	if errp != nil {
		return shim.Error(errp.Error())
	}

	errp = json.Unmarshal([]byte(args[0]), &stsType)
	if errp != nil {
		return shim.Error(errp.Error())
	}


	val,err:=stub.GetState(partial.StatusTypeKey)
	if err!=nil{
		return shim.Error(err.Error())
	}

	if val == nil {
		return shim.Error("Asset type not found \n")
	}

	err=stub.DelState(partial.StatusTypeKey)
	if err!=nil{
		return shim.Error(err.Error())
	}

	// key for the ledger
	statusTypeKey, err := stub.CreateCompositeKey("StatusType", []string{stsType.StatusType})
	if err != nil {
		return shim.Error(err.Error())
	}

	


	//Marshal the data and put into the ledger

	statusAsBytes, err := json.Marshal(stsType)
	if err != nil {
		return shim.Error("Error marshaling asset type structure")
	}
	//Put the key value pair in ledger
	err = stub.PutState(statusTypeKey, statusAsBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(statusAsBytes)

}
//DeleteStatusType allows to create status for request
func DeleteStatusType(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//Check for attribute permission
	// err := cid.AssertAttributeValue(stub,"CAN_CREATE_STATUSTYPE","true")
	// if err != nil {
	// 	return shim.Error(err.Error())
	// }

	//Check for correct number of arguments
	if len(args) != 1 {
		return shim.Error("Invalid argument count\n")
	}

	//Take the required structs from the argument
	partial := struct {
		StatusTypeKey string `json:"statusTypeKey"`
	}{}
		
	errp := json.Unmarshal([]byte(args[0]), &partial)
	if errp != nil {
		return shim.Error(errp.Error())
	}


	
err:= stub.DelState(partial.StatusTypeKey)
if err!=nil{
	return shim.Error(err.Error())
}

	return shim.Success(nil)

}




//GetStatusType returns all the defined sex values
func GetStatusType(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//Check for attribute permission
	// err := cid.AssertAttributeValue(stub,"CAN_READ_STATUSTYPE","true")
	// if err != nil {
	// 	return shim.Error(err.Error())
	// }

	if len(args) != 0 {
		return shim.Error("Expected no arguments")
	}

	resultsIterator, err := stub.GetStateByPartialCompositeKey("StatusType", []string{})
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	results := []interface{}{}
	for resultsIterator.HasNext() {
		kvResult, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		//Construct response struct
		result := struct {
			StatusTypeKey string `json:"statusTypeKey"`
			StatusType   string `json:"statusType"`
			Disable   bool `json:"disable"`
		}{}

		err = json.Unmarshal(kvResult.Value, &result)
		if err != nil {
			return shim.Error(err.Error())
		}
		if result.Disable==true{
			continue
		}

		//Fetch Key
		result.StatusTypeKey = kvResult.Key
		results = append(results, result)
	}
	resultsAsBytes, err := json.Marshal(results)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(resultsAsBytes)
}

