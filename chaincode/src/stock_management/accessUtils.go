package main

import (
	"crypto/x509"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/lib/cid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

func getTxCreatorInfo(stub shim.ChaincodeStubInterface) (string, string, error) {
	var mspid string
	var err error
	var cert *x509.Certificate

	mspid, err = cid.GetMSPID(stub)
	if err != nil {
		fmt.Printf("Error getting MSP identity: %s\n", err.Error())
		return "", "", err
	}

	cert, err = cid.GetX509Certificate(stub)
	if err != nil {
		fmt.Printf("Error getting client certificate: %s\n", err.Error())
		return "", "", err
	}

	return mspid, cert.Issuer.CommonName, nil
}

// For now, just hardcode an ACL
// We will support attribute checks in an upgrade

func authenticateEmployeeEntityOrg(stub shim.ChaincodeStubInterface) (bool, error) {
	mspID, certCN, err := getTxCreatorInfo(stub)
	if err != nil {
		return false, err
	}
	return (mspID == "epMSP") && (certCN == "ca.ep.demo.com"), nil
}

func authenticateAdministrationOrg(stub shim.ChaincodeStubInterface) (bool, error) {
	mspID, certCN, err := getTxCreatorInfo(stub)
	if err != nil {
		return false, err
	}
	return (mspID == "adMSP") && (certCN == "ca.ad.demo.com"), nil
}

func authenticateOperationsOrg(stub shim.ChaincodeStubInterface) (bool, error) {
	mspID, certCN, err := getTxCreatorInfo(stub)
	if err != nil {
		return false, err
	}
	return (mspID == "odMSP") && (certCN == "ca.od.demo.com"), nil
}
