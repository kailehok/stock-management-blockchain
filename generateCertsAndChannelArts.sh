#!/bin/bash

#generate certs
cryptogen generate --config=./crypto-config.yaml

export FABRIC_CFG_PATH=$PWD
export CHANNEL_NAME=demo-channel
configtxgen -profile ThreeOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block
configtxgen -profile ThreeOrgsChannel -outputCreateChannelTx ./channel-artifacts/${CHANNEL_NAME}.tx -channelID $CHANNEL_NAME

#anchor peers update transaction
configtxgen -profile ThreeOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/adMSPanchors.tx -channelID $CHANNEL_NAME -asOrg adMSP
configtxgen -profile ThreeOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/odMSPanchors.tx -channelID $CHANNEL_NAME -asOrg odMSP
configtxgen -profile ThreeOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/epMSPanchors.tx -channelID $CHANNEL_NAME -asOrg epMSP