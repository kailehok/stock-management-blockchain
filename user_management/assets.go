package main


//Permission info
type Permission struct{
	DocType string `json:"docType"`
	Permissions []string `json:"permissions"`
}
//SuperAdmin Info
type SuperAdmin struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	Password string `json:"password"`
	Orgname string `json:"orgname"`
	Username string `json:"username"`
}

//Admin Info
type Admin struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	Password string `json:"password"`
	Orgname string `json:"orgname"`
	Username string `json:"username"`
	DateOfBirth int `json:"dateOfBirth"`
	Address string `json:"address"`
	MobileNo string `json:"mobileNo"`
	EmailID string `json:"emailID"`
	Group string `json:"group"`
}
//User Info 
type User struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	Password string `json:"password"`
	Orgname string `json:"orgname"`
	Username string `json:"username"`
	DateOfBirth int `json:"dateOfBirth"`
	Address string `json:"address"`
	MobileNo string `json:"mobileNo"`
	EmailID string `json:"emailID"`
	Group string `json:"group"`
	
	



}
//Group info
type Group struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	Permissions []string `json:"permissions"`
}