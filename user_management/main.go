package main

import (
	"context"
	"fmt"
	"net/http"

	_ "github.com/go-kivik/couchdb" // The CouchDB driver
	"github.com/go-kivik/kivik"     // Development version of Kivik
	"github.com/gorilla/mux"
)

//db1,db2,db3 database
var db1, db2, db3 *kivik.DB

//flow variable
var flow = map[string]*kivik.DB{}

func main() {

	client, err := kivik.New("couch", "http://103.233.56.101:5048")
	if err != nil {
		panic(err)
	}

	db1 = client.DB(context.TODO(), "users")
	flow["ep"] = db1

	db2 = client.DB(context.TODO(), "user")

	db3 = client.DB(context.TODO(), "userss")
	flow["ad"] = db2
	flow["od"] = db3
	if err != nil {
		panic(err)
	}

	router := mux.NewRouter()
	fmt.Println("outside")
	router.HandleFunc("/super-admin", createSuperAdmin).Methods("POST")

	router.HandleFunc("/login", login).Methods("POST")

	router.HandleFunc("/group", createGroup).Methods("POST")
	router.HandleFunc("/group", readGroup).Methods("GET")
	// router.HandleFunc("/group/{id}", updateGroup).Methods("PUT")
	// router.HandleFunc("/group/{id}", deleteGroup).Methods("DELETE")

	router.HandleFunc("/admin", createAdmin).Methods("POST")
	router.HandleFunc("/admin", readAdmin).Methods("GET")
	// router.HandleFunc("/admin/{id}", updateAdmin).Methods("PUT")
	// router.HandleFunc("/admin/{id}", deleteAdmin).Methods("DELETE")

	router.HandleFunc("/user", createUser).Methods("POST")
	router.HandleFunc("/user", readUser).Methods("GET")
	// router.HandleFunc("/user/{id}", updateUser).Methods("PUT")
	// router.HandleFunc("/user/{id}", deleteUser).Methods("DELETE")

	router.HandleFunc("/permissions", createPermission).Methods("POST")

	router.HandleFunc("/permissions", readPermission).Methods("GET")
	// router.HandleFunc("/assettype/{id}", updateAssetType).Methods("PUT")
	// router.HandleFunc("/assettype/{id}", deleteAssetType).Methods("DELETE")
	// router.HandleFunc("/assettype", readAssetType).Methods("GET")
	// router.HandleFunc("/asset", createAsset).Methods("POST")
	// router.HandleFunc("/asset/{id}", updateAsset).Methods("PUT")
	// router.HandleFunc("/asset/{id}", deleteAsset).Methods("DELETE")
	// router.HandleFunc("/asset", readAsset).Methods("GET")

	err = http.ListenAndServe(":8080", router)
	if err != nil {
		fmt.Println("inside")
		panic(err)
	}
}
