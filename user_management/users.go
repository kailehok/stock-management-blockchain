package main

import (
	"context"
	"errors"
	"strings"
	"time"

	// "github.com/go-kivik/kivik"
	"github.com/gorilla/mux"
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-kivik/couchdb" // The CouchDB driver
	"github.com/go-kivik/kivik"

	jwt "github.com/dgrijalva/jwt-go"
)

var mySigningKey = []byte("captainjacksparrowsayshi")

//createSuperAdmin creates super admin
func createSuperAdmin(w http.ResponseWriter, r *http.Request) {
	var superAdmin SuperAdmin
	err := json.NewDecoder(r.Body).Decode(&superAdmin)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	superAdmin.DocType = "SuperAdmin"

	docMap, err := ToJSONCompatibleMap(superAdmin)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	_, err = flow[superAdmin.Orgname].Put(context.TODO(), "User"+superAdmin.Username, docMap)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, superAdmin)
	//fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}

//login function 

func login(w http.ResponseWriter, r *http.Request) {
	partial := struct {
		Username string `json:"username"`
		Password string `json:"password"`
		Orgname  string `json:"orgname"`
	}{}
	err := json.NewDecoder(r.Body).Decode(&partial)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	//Check username exists
	response:=struct{
		Success bool `json:"success"`
		Message string `json:"message"`
	}{}

	row:=flow[partial.Orgname].Get(context.TODO(),"User"+partial.Username)
	if row.Rev==""{
	fmt.Println("Error")
	response.Success=false
	response.Message="User not authorized!"

	respondJSON(w, http.StatusUnauthorized, response)
	return
     
	}

	partial1:=struct{
		Password string `json:"password"`
	}{}
	row.ScanDoc(&partial1)
	if (partial1.Password != partial.Password){
		response.Success=false
	response.Message="Incorrect password!"

	respondJSON(w, http.StatusUnauthorized, response)
	return
	}

	token, err := GenerateJWT(partial.Username, partial.Orgname)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())

	}
	response.Success=true
	response.Message=token
	fmt.Println(response)

	respondJSON(w, http.StatusOK, response)
	return




	


}

//createPermission function
func createPermission(w http.ResponseWriter, r *http.Request) {

	var permission Permission
	err := json.NewDecoder(r.Body).Decode(&permission)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	permission.DocType = "Permission"

	docMap, err := ToJSONCompatibleMap(permission)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	_, err = db1.Put(context.TODO(), "Permission", docMap)
	_, err = db2.Put(context.TODO(), "Permission", docMap)
	_, err = db3.Put(context.TODO(), "Permission", docMap)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, permission)
	//fmt.Printf("Type document is inserted with revision %s \n", rev)

	return
}

//createGroup struct
func createGroup(w http.ResponseWriter, r *http.Request) {
	tokenString, err := Validation(r)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])
	check:=checkPermission(claims["username"].(string),claims["orgname"].(string),"CAN_CREATE_GROUP")
	if !check {
		respondError(w, http.StatusInternalServerError, "Permission Denied")
		return
	}


	row:=flow[claims["orgname"].(string)].Get(context.TODO(),"User"+claims["username"].(string))
	if row.Rev==""{
	fmt.Println("Error")
	respondError(w, http.StatusInternalServerError, "User not found")
     return 
	}
	partial:=struct{
		DocType string `json:"docType"`
	}{}
	row.ScanDoc(&partial)
	fmt.Println(partial.DocType)
	

	var group Group
	err = json.NewDecoder(r.Body).Decode(&group)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	if partial.DocType=="SuperAdmin"{
		group.DocType = "AdminGroup"
	} else if partial.DocType=="Admin"{
		group.DocType="UserGroup"
	}
    
	

	docMap, err := ToJSONCompatibleMap(group)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	_, err = flow[claims["orgname"].(string)].Put(context.TODO(), "Group"+group.Name, docMap)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	fmt.Println(group)
	respondJSON(w, http.StatusCreated, group)
	//fmt.Printf("Type document is inserted with revision %s \n", rev)

	return
}

//readGroup struct
func readGroup(w http.ResponseWriter, r *http.Request) {
	tokenString, err := Validation(r)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])

	rows, err := flow[claims["orgname"].(string)].Query(context.TODO(), "query", "group", kivik.Options{"include_docs": true})
	if err != nil {
		fmt.Println("there")
		respondError(w, http.StatusInternalServerError, err.Error())
	 return
	}

	fmt.Println("i dont see")
	row:=flow[claims["orgname"].(string)].Get(context.TODO(),"User"+claims["username"].(string))
	if row.Rev==""{
	fmt.Println("Error")
	respondError(w, http.StatusInternalServerError, "User not found")
     return 
	}
	partial:=struct{
		DocType string `json:"docType"`
	}{}
	row.ScanDoc(&partial)
	
	test := struct {
		ID          string   `json:"_id"`
		DocType     string   `json:"docType"`
		Name        string   `json:"name"`
		Permissions []string `json:"permissions"`
	}{}
	results := []interface{}{}
	for rows.Next() {

		if err := rows.ScanDoc(&test); err != nil {
			respondError(w, http.StatusInternalServerError, err.Error())
			return
		}
		if partial.DocType=="SuperAdmin" && test.DocType!="AdminGroup"{
			continue
		} else if partial.DocType=="Admin" && test.DocType!="UserGroup"{
			continue
		}
		results = append(results, test)
		fmt.Println(results)
	}

	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	// doc,_:=ToJSONCompatibleMap(results)
	fmt.Println(results)
	respondJSON(w, http.StatusCreated, results)
	return

}

//updateGroup creates type
func updateGroup(w http.ResponseWriter, r *http.Request) {
	fmt.Println("update")
	tokenString, err := Validation(r)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])
	check:=checkPermission(claims["username"].(string),claims["orgname"].(string),"CAN_UPDATE_GROUP")
	if !check{
		respondError(w, http.StatusInternalServerError, "Permission Denied")
		return
	}

	params:=mux.Vars(r)
	id:=params["id"]

	row:=flow[claims["orgname"].(string)].Get(context.TODO(),"Group"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	

	var group Group
	_ = json.NewDecoder(r.Body).Decode(&group)

	var doc map[string]interface{}

	if id=="Group"+ group.Name{
		doc= map[string]interface{}{
			"_rev":row.Rev,
			"docType": "Group",
			"name": group.Name,
			"permissions":group.Permissions,

		}

	}else { 
		doc= map[string]interface{}{
			
			"docType": "Group",
			"name": group.Name,
			"permissions":group.Permissions,


		}
	}





	
	rev,err := flow[claims["orgname"].(string)].Delete(context.TODO(),"Group"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rev, err = flow[claims["orgname"].(string)].Put(context.TODO(),"Group"+group.Name, doc)
	if err != nil {
		fmt.Println("Error")

		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, doc)
	fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}

//deleteGroup deletes group
func deleteGroup(w http.ResponseWriter, r *http.Request) {
	fmt.Println("delete")

	tokenString, err := Validation(r)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])
	check:=checkPermission(claims["username"].(string),claims["orgname"].(string),"CAN_DELETE_GROUP")
	if !check{
		respondError(w, http.StatusInternalServerError, "Permission Denied")
		return
	}

	params:=mux.Vars(r)
	id:=params["id"]

	row:=flow[claims["orgname"].(string)].Get(context.TODO(),"Group"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	_,err = flow[claims["orgname"].(string)].Delete(context.TODO(),"Group"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK,"Sucessfully deleted!")
	return
}


//createAdmin creates type
func createAdmin(w http.ResponseWriter, r *http.Request) {

	tokenString, err := Validation(r)
	claims, _ := ExtractClaims(tokenString)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	// partial:=struct{
	// 	GroupName string `json:"groupName"`

	// }{}
	// err = json.NewDecoder(r.Body).Decode(&partial)
	// if err != nil {
	// 	respondError(w, http.StatusInternalServerError, err.Error())
	// 	return
	// }

	// row:=flow[claims["orgname"].(string)].Get(context.TODO(),"Group"+partial.GroupName)
	// if row.Rev==""{
	// fmt.Println("Error")

	// 	respondJSON(w, http.StatusNotFound,"Incorrect group ("+partial.GroupName+") provided!")
	// 	return
	// }
	// var group Group
	// row.ScanDoc(&group)
	// fmt.Println(group)

	var user Admin
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	user.DocType = "Admin"
	user.Orgname = claims["orgname"].(string)

	docMap, err := ToJSONCompatibleMap(user)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	_, err = flow[claims["orgname"].(string)].Put(context.TODO(), "User"+user.Username, docMap)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, user)
	//fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}

//readAdmin struct
func readAdmin(w http.ResponseWriter, r *http.Request) {
	tokenString, err := Validation(r)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])

	rows, err := flow[claims["orgname"].(string)].Query(context.TODO(), "query", "admin", kivik.Options{"include_docs": true})
	if err != nil {
		panic(err)
	}

	fmt.Println("i dont see")
	test := struct {
		ID          string `json:"_id"`
		DocType     string `json:"docType"`
		Name        string `json:"name"`
		Password    string `json:"password"`
		Orgname     string `json:"orgname"`
		Username    string `json:"username"`
		DateOfBirth int    `json:"dateOfBirth"`
		Address     string `json:"address"`
		MobileNo    string `json:"mobileNo"`
		EmailID     string `json:"emailID"`
		Group       string `json:"group"`
	}{}
	results := []interface{}{}
	for rows.Next() {

		if err := rows.ScanDoc(&test); err != nil {
			panic(err)
		}
		results = append(results, test)
		fmt.Println(results)
	}

	if err != nil {
		panic(err)
	}

	// doc,_:=ToJSONCompatibleMap(results)
	fmt.Println(results)
	respondJSON(w, http.StatusCreated, results)
	return

}


//updateAdmin creates type
func updateAdmin(w http.ResponseWriter, r *http.Request) {
	fmt.Println("update")
	tokenString, err := Validation(r)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])
	check:=checkPermission(claims["username"].(string),claims["orgname"].(string),"CAN_UPDATE_ADMIN")
	if !check{
		respondError(w, http.StatusInternalServerError, "Permission Denied")
		return
	}

	params:=mux.Vars(r)
	id:=params["id"]

	row:=flow[claims["orgname"].(string)].Get(context.TODO(),"User"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	

	var user Admin
	_ = json.NewDecoder(r.Body).Decode(&user)

	var doc map[string]interface{}

	if id=="User"+ user.Username{
		doc= map[string]interface{}{
			"_rev":row.Rev,
		"docType":"Admin",
			"name":user.Name,
			"password":user.Password,
			"orgname":user.Orgname,
			"username":user.Username,
			"dateOfBirth":user.DateOfBirth,
			"address":user.Address,
			"mobileNo":user.MobileNo,
			"emailID":user.EmailID,
			"group":user.Group,

		}

	}else { 
		doc= map[string]interface{}{
			
			"docType":"Admin",
			"name":user.Name,
			"password":user.Password,
			"orgname":user.Orgname,
			"username":user.Username,
			"dateOfBirth":user.DateOfBirth,
			"address":user.Address,
			"mobileNo":user.MobileNo,
			"emailID":user.EmailID,
			"group":user.Group,


		}
	}





	
	rev,err := flow[claims["orgname"].(string)].Delete(context.TODO(),"User"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rev, err = flow[claims["orgname"].(string)].Put(context.TODO(),"User"+user.Username, doc)
	if err != nil {
		fmt.Println("Error")

		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, doc)
	fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}

//deleteAdmin deletes admin
func deleteAdmin(w http.ResponseWriter, r *http.Request) {
	fmt.Println("delete")

	tokenString, err := Validation(r)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])
	check:=checkPermission(claims["username"].(string),claims["orgname"].(string),"CAN_DELETE_ADMIN")
	if !check{
		respondError(w, http.StatusInternalServerError, "Permission Denied")
		return
	}

	params:=mux.Vars(r)
	id:=params["id"]

	row:=flow[claims["orgname"].(string)].Get(context.TODO(),"User"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	_,err = flow[claims["orgname"].(string)].Delete(context.TODO(),"User"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK,"Sucessfully deleted!")
	return
}


//createUser creates users
func createUser(w http.ResponseWriter, r *http.Request) {

	tokenString, err := Validation(r)
	claims, _ := ExtractClaims(tokenString)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}

	var user User
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	user.DocType = "User"

	docMap, err := ToJSONCompatibleMap(user)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	_, err = flow[claims["orgname"].(string)].Put(context.TODO(), "User"+user.Username, docMap)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, user)
	//fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}

//readUser struct
func readUser(w http.ResponseWriter, r *http.Request) {
	tokenString, err := Validation(r)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])

	rows, err := flow[claims["orgname"].(string)].Query(context.TODO(), "query", "user", kivik.Options{"include_docs": true})
	if err != nil {
		panic(err)
	}

	fmt.Println("i dont see")
	test := struct {
		ID          string   `json:"_id"`
		Name        string   `json:"name"`
		Password    string   `json:"password"`
		Orgname     string   `json:"orgname"`
		Username    string   `json:"username"`
		DateOfBirth int      `json:"dateOfBirth"`
		Address     string   `json:"address"`
		MobileNo    string   `json:"mobileNo"`
		EmailID     string   `json:"emailID"`
		Group string `json:"group"`
	}{}
	results := []interface{}{}
	for rows.Next() {

		if err := rows.ScanDoc(&test); err != nil {
			panic(err)
		}
		results = append(results, test)
		fmt.Println(results)
	}

	if err != nil {
		panic(err)
	}

	// doc,_:=ToJSONCompatibleMap(results)
	fmt.Println(results)
	respondJSON(w, http.StatusCreated, results)
	return

}


//updateUser creates type
func updateUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("update")
	tokenString, err := Validation(r)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])
	check:=checkPermission(claims["username"].(string),claims["orgname"].(string),"CAN_UPDATE_USER")
	if !check{
		respondError(w, http.StatusInternalServerError, "Permission Denied")
		return
	}

	params:=mux.Vars(r)
	id:=params["id"]

	row:=flow[claims["orgname"].(string)].Get(context.TODO(),"User"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	

	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)

	var doc map[string]interface{}

	if id=="User"+ user.Username{
		doc= map[string]interface{}{
			"_rev":row.Rev,
		"docType":"Admin",
			"name":user.Name,
			"password":user.Password,
			"orgname":user.Orgname,
			"username":user.Username,
			"dateOfBirth":user.DateOfBirth,
			"address":user.Address,
			"mobileNo":user.MobileNo,
			"emailID":user.EmailID,
			"group": user.Group,

		}

	}else { 
		doc= map[string]interface{}{
			
			"docType":"Admin",
			"name":user.Name,
			"password":user.Password,
			"orgname":user.Orgname,
			"username":user.Username,
			"dateOfBirth":user.DateOfBirth,
			"address":user.Address,
			"mobileNo":user.MobileNo,
			"emailID":user.EmailID,
			"permissions":user.Group,


		}
	}





	
	rev,err := flow[claims["orgname"].(string)].Delete(context.TODO(),"User"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rev, err = flow[claims["orgname"].(string)].Put(context.TODO(),"User"+user.Username, doc)
	if err != nil {
		fmt.Println("Error")

		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, doc)
	fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}

//deleteUser deletes admin
func deleteUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("delete")

	tokenString, err := Validation(r)
	if err != nil {
		fmt.Println(err.Error())
		respondError(w, http.StatusInternalServerError, err.Error())
		return

	}
	claims, _ := ExtractClaims(tokenString)
	fmt.Println(claims["username"])
	fmt.Println(claims["orgname"])
	check:=checkPermission(claims["username"].(string),claims["orgname"].(string),"CAN_DELETE_USER")
	if !check{
		respondError(w, http.StatusInternalServerError, "Permission Denied")
		return
	}

	params:=mux.Vars(r)
	id:=params["id"]

	row:=flow[claims["orgname"].(string)].Get(context.TODO(),"User"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	_,err = flow[claims["orgname"].(string)].Delete(context.TODO(),"User"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK,"Sucessfully deleted!")
	return
}


// respondJSON makes the response with payload as json format
func respondJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}

// respondError makes the error response with payload as json format
func respondError(w http.ResponseWriter, code int, message string) {
	respondJSON(w, code, map[string]string{"error": message})
}

//ToJSONCompatibleMap function
func ToJSONCompatibleMap(obj interface{}) (map[string]interface{}, error) {
	objAsBytes, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	var objAsMap map[string]interface{}
	err = json.Unmarshal(objAsBytes, &objAsMap)
	if err != nil {
		return nil, err
	}
	return objAsMap, nil
}

//GenerateJWT function
func GenerateJWT(username, orgname string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["username"] = username
	claims["orgname"] = orgname
	claims["exp"] = time.Now().Add(time.Minute * 1000).Unix()

	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		err := fmt.Errorf("Something Went Wrong: %s", err.Error())
		return "", err
	}

	return tokenString, nil
}

//ExtractClaims function
func ExtractClaims(tokenStr string) (jwt.MapClaims, bool) {

	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// check token signing method etc
		return mySigningKey, nil
	})

	if err != nil {
		return nil, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, true
	}
	return nil, true
}

//Validation token
func Validation(r *http.Request) (string, error) {

	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer ")
	reqToken = splitToken[1]

	if reqToken != "" {

		token, err := jwt.Parse(reqToken, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("There was an error")
			}
			return mySigningKey, nil
		})

		if err != nil {
			return "", err
		}

		if token.Valid {
			return reqToken, nil
		}
	}

	return "", errors.New("Invalid token")

}

//readPermission function
func readPermission(w http.ResponseWriter, r *http.Request) {

	tokenString, err := Validation(r)
	claims, _ := ExtractClaims(tokenString)
	if err != nil {
		fmt.Println(err.Error())
		return

	}

	row := flow[claims["orgname"].(string)].Get(context.TODO(), "User"+claims["username"].(string))
	if row.Rev == "" {
		fmt.Println("Error")

		respondJSON(w, http.StatusNotFound, "Incorrect info provided!")
		return
	}
	partial := struct {
		DocType string `json:"docType"`
	}{}
	row.ScanDoc(&partial)
	fmt.Println(partial.DocType)
	if partial.DocType == "SuperAdmin" {
		row := flow[claims["orgname"].(string)].Get(context.TODO(), "Permission")
		if row.Rev == "" {
			fmt.Println("Error")

			respondJSON(w, http.StatusNotFound, "No permission provided!")
			return
		}
		partial := struct {
			Permissions []string `json:"permissions"`
		}{}
		row.ScanDoc(&partial)
		respondJSON(w, http.StatusCreated, partial)
		return

	} else if partial.DocType == "Admin" || partial.DocType=="User"{
		row := flow[claims["orgname"].(string)].Get(context.TODO(), "User"+claims["username"].(string))
		if row.Rev == "" {
			fmt.Println("Error")

			respondJSON(w, http.StatusNotFound, "No permission provided!")
			return
		}
		partial := struct {
			Group string `json:"group"`
		}{}
		row.ScanDoc(&partial)
		row = flow[claims["orgname"].(string)].Get(context.TODO(), "Group"+partial.Group)
		if row.Rev == "" {
			fmt.Println("Error")

			respondJSON(w, http.StatusNotFound, "No permission provided!")
			return
		}
		partial1 := struct {
			Permissions []string `json:"permissions"`
		}{}
		row.ScanDoc(&partial1)
		respondJSON(w, http.StatusCreated, partial1)
		return
	}

	// } else if partial.DocType == "User" {
	// 	row := flow[claims["orgname"].(string)].Get(context.TODO(), "User"+claims["username"].(string))
	// 	if row.Rev == "" {
	// 		fmt.Println("Error")

	// 		respondJSON(w, http.StatusNotFound, "No permission provided!")
	// 		return
	// 	}

	// 	partial := struct {
	// 		Permissions []string `json:"permissions"`
	// 	}{}
	// 	row.ScanDoc(&partial)
	// 	respondJSON(w, http.StatusCreated, partial)
	// 	return
	// }

	return

}
