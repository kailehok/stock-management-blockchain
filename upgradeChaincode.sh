#upgrade chaincode "mycc" to the version specified
set -x

if [ $1 ]; then
  # install on every peer on channel
  docker exec ad-cli peer chaincode install -n mycc -v $1 -p github.com/chaincode/src/stock_management
  docker exec -e CORE_PEER_ADDRESS=peer1.ad.demo.com:7051 ad-cli peer chaincode install -n mycc -v $1 -p github.com/chaincode/src/stock_management
  docker exec -e CORE_PEER_ADDRESS=peer0.od.demo.com:7051 -e CORE_PEER_LOCALMSPID=odMSP -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/crypto/peerOrganizations/od.demo.com/users/Admin@od.demo.com/msp ad-cli peer chaincode install -n mycc -v $1 -p github.com/chaincode/src/stock_management
  docker exec -e CORE_PEER_ADDRESS=peer0.ep.demo.com:7051 -e CORE_PEER_LOCALMSPID=epMSP -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/crypto/peerOrganizations/ep.demo.com/users/Admin@ep.demo.com/msp ad-cli peer chaincode install -n mycc -v $1 -p github.com/chaincode/src/stock_management

  # upgrade
  docker exec ad-cli peer chaincode upgrade -C demo-channel -n mycc -v $1 -p github.com/chaincode/src/stock_management -c  '{"Args":[]}'
else
  echo "pass chaincode version as first arg"
fi